﻿namespace Question_Bank
{
    partial class Admin_Panel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Admin_Panel));
            this.lbl_ID = new System.Windows.Forms.Label();
            this.lbl_question = new System.Windows.Forms.Label();
            this.lbl_A = new System.Windows.Forms.Label();
            this.lbl_B = new System.Windows.Forms.Label();
            this.lbl_C = new System.Windows.Forms.Label();
            this.lbl_D = new System.Windows.Forms.Label();
            this.lbl_Answer = new System.Windows.Forms.Label();
            this.txt_ID = new System.Windows.Forms.TextBox();
            this.txt_question = new System.Windows.Forms.TextBox();
            this.txt_A = new System.Windows.Forms.TextBox();
            this.txt_B = new System.Windows.Forms.TextBox();
            this.txt_C = new System.Windows.Forms.TextBox();
            this.txt_D = new System.Windows.Forms.TextBox();
            this.txt_Answer = new System.Windows.Forms.TextBox();
            this.btn_INSERT = new System.Windows.Forms.Button();
            this.btn_UPDATE = new System.Windows.Forms.Button();
            this.btn_DELETE = new System.Windows.Forms.Button();
            this.btn_LOAD = new System.Windows.Forms.Button();
            this.DGV_table = new System.Windows.Forms.DataGridView();
            this.comboBox_table = new System.Windows.Forms.ComboBox();
            this.btn_time_question = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGV_table)).BeginInit();
            this.SuspendLayout();
            // 
            // lbl_ID
            // 
            this.lbl_ID.AutoSize = true;
            this.lbl_ID.BackColor = System.Drawing.Color.Transparent;
            this.lbl_ID.ForeColor = System.Drawing.Color.Yellow;
            this.lbl_ID.Location = new System.Drawing.Point(27, 12);
            this.lbl_ID.Name = "lbl_ID";
            this.lbl_ID.Size = new System.Drawing.Size(18, 13);
            this.lbl_ID.TabIndex = 0;
            this.lbl_ID.Text = "ID";
            // 
            // lbl_question
            // 
            this.lbl_question.AutoSize = true;
            this.lbl_question.BackColor = System.Drawing.Color.Transparent;
            this.lbl_question.ForeColor = System.Drawing.Color.Yellow;
            this.lbl_question.Location = new System.Drawing.Point(27, 49);
            this.lbl_question.Name = "lbl_question";
            this.lbl_question.Size = new System.Drawing.Size(49, 13);
            this.lbl_question.TabIndex = 1;
            this.lbl_question.Text = "Question";
            // 
            // lbl_A
            // 
            this.lbl_A.AutoSize = true;
            this.lbl_A.BackColor = System.Drawing.Color.Transparent;
            this.lbl_A.ForeColor = System.Drawing.Color.Yellow;
            this.lbl_A.Location = new System.Drawing.Point(31, 126);
            this.lbl_A.Name = "lbl_A";
            this.lbl_A.Size = new System.Drawing.Size(14, 13);
            this.lbl_A.TabIndex = 2;
            this.lbl_A.Text = "A";
            // 
            // lbl_B
            // 
            this.lbl_B.AutoSize = true;
            this.lbl_B.BackColor = System.Drawing.Color.Transparent;
            this.lbl_B.ForeColor = System.Drawing.Color.Yellow;
            this.lbl_B.Location = new System.Drawing.Point(31, 163);
            this.lbl_B.Name = "lbl_B";
            this.lbl_B.Size = new System.Drawing.Size(14, 13);
            this.lbl_B.TabIndex = 3;
            this.lbl_B.Text = "B";
            // 
            // lbl_C
            // 
            this.lbl_C.AutoSize = true;
            this.lbl_C.BackColor = System.Drawing.Color.Transparent;
            this.lbl_C.ForeColor = System.Drawing.Color.Yellow;
            this.lbl_C.Location = new System.Drawing.Point(31, 199);
            this.lbl_C.Name = "lbl_C";
            this.lbl_C.Size = new System.Drawing.Size(14, 13);
            this.lbl_C.TabIndex = 4;
            this.lbl_C.Text = "C";
            // 
            // lbl_D
            // 
            this.lbl_D.AutoSize = true;
            this.lbl_D.BackColor = System.Drawing.Color.Transparent;
            this.lbl_D.ForeColor = System.Drawing.Color.Yellow;
            this.lbl_D.Location = new System.Drawing.Point(31, 235);
            this.lbl_D.Name = "lbl_D";
            this.lbl_D.Size = new System.Drawing.Size(15, 13);
            this.lbl_D.TabIndex = 5;
            this.lbl_D.Text = "D";
            // 
            // lbl_Answer
            // 
            this.lbl_Answer.AutoSize = true;
            this.lbl_Answer.BackColor = System.Drawing.Color.Transparent;
            this.lbl_Answer.ForeColor = System.Drawing.Color.Yellow;
            this.lbl_Answer.Location = new System.Drawing.Point(27, 271);
            this.lbl_Answer.Name = "lbl_Answer";
            this.lbl_Answer.Size = new System.Drawing.Size(42, 13);
            this.lbl_Answer.TabIndex = 6;
            this.lbl_Answer.Text = "Answer";
            // 
            // txt_ID
            // 
            this.txt_ID.BackColor = System.Drawing.Color.Silver;
            this.txt_ID.Location = new System.Drawing.Point(98, 12);
            this.txt_ID.Name = "txt_ID";
            this.txt_ID.Size = new System.Drawing.Size(100, 20);
            this.txt_ID.TabIndex = 7;
            // 
            // txt_question
            // 
            this.txt_question.Location = new System.Drawing.Point(98, 38);
            this.txt_question.Multiline = true;
            this.txt_question.Name = "txt_question";
            this.txt_question.Size = new System.Drawing.Size(811, 76);
            this.txt_question.TabIndex = 8;
            this.txt_question.TextChanged += new System.EventHandler(this.txt_question_TextChanged);
            // 
            // txt_A
            // 
            this.txt_A.Location = new System.Drawing.Point(98, 126);
            this.txt_A.Multiline = true;
            this.txt_A.Name = "txt_A";
            this.txt_A.Size = new System.Drawing.Size(211, 31);
            this.txt_A.TabIndex = 9;
            // 
            // txt_B
            // 
            this.txt_B.Location = new System.Drawing.Point(98, 163);
            this.txt_B.Multiline = true;
            this.txt_B.Name = "txt_B";
            this.txt_B.Size = new System.Drawing.Size(211, 30);
            this.txt_B.TabIndex = 10;
            // 
            // txt_C
            // 
            this.txt_C.Location = new System.Drawing.Point(98, 199);
            this.txt_C.Multiline = true;
            this.txt_C.Name = "txt_C";
            this.txt_C.Size = new System.Drawing.Size(211, 30);
            this.txt_C.TabIndex = 11;
            // 
            // txt_D
            // 
            this.txt_D.Location = new System.Drawing.Point(98, 235);
            this.txt_D.Multiline = true;
            this.txt_D.Name = "txt_D";
            this.txt_D.Size = new System.Drawing.Size(211, 30);
            this.txt_D.TabIndex = 12;
            // 
            // txt_Answer
            // 
            this.txt_Answer.Location = new System.Drawing.Point(98, 271);
            this.txt_Answer.Multiline = true;
            this.txt_Answer.Name = "txt_Answer";
            this.txt_Answer.Size = new System.Drawing.Size(211, 30);
            this.txt_Answer.TabIndex = 13;
            this.txt_Answer.TextChanged += new System.EventHandler(this.textBox7_TextChanged);
            // 
            // btn_INSERT
            // 
            this.btn_INSERT.Location = new System.Drawing.Point(98, 465);
            this.btn_INSERT.Name = "btn_INSERT";
            this.btn_INSERT.Size = new System.Drawing.Size(87, 39);
            this.btn_INSERT.TabIndex = 14;
            this.btn_INSERT.Text = "INSERT";
            this.btn_INSERT.UseVisualStyleBackColor = true;
            this.btn_INSERT.Click += new System.EventHandler(this.btn_INSERT_Click);
            // 
            // btn_UPDATE
            // 
            this.btn_UPDATE.Location = new System.Drawing.Point(316, 465);
            this.btn_UPDATE.Name = "btn_UPDATE";
            this.btn_UPDATE.Size = new System.Drawing.Size(82, 39);
            this.btn_UPDATE.TabIndex = 15;
            this.btn_UPDATE.Text = "UPDATE";
            this.btn_UPDATE.UseVisualStyleBackColor = true;
            this.btn_UPDATE.Click += new System.EventHandler(this.btn_UPDATE_Click);
            // 
            // btn_DELETE
            // 
            this.btn_DELETE.Location = new System.Drawing.Point(510, 465);
            this.btn_DELETE.Name = "btn_DELETE";
            this.btn_DELETE.Size = new System.Drawing.Size(81, 39);
            this.btn_DELETE.TabIndex = 16;
            this.btn_DELETE.Text = "DELETE";
            this.btn_DELETE.UseVisualStyleBackColor = true;
            this.btn_DELETE.Click += new System.EventHandler(this.btn_DELETE_Click);
            // 
            // btn_LOAD
            // 
            this.btn_LOAD.Location = new System.Drawing.Point(712, 465);
            this.btn_LOAD.Name = "btn_LOAD";
            this.btn_LOAD.Size = new System.Drawing.Size(90, 39);
            this.btn_LOAD.TabIndex = 17;
            this.btn_LOAD.Text = " Load";
            this.btn_LOAD.UseVisualStyleBackColor = true;
            this.btn_LOAD.Click += new System.EventHandler(this.btn_LOAD_Click);
            // 
            // DGV_table
            // 
            this.DGV_table.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGV_table.Location = new System.Drawing.Point(392, 126);
            this.DGV_table.Name = "DGV_table";
            this.DGV_table.Size = new System.Drawing.Size(755, 318);
            this.DGV_table.TabIndex = 18;
            this.DGV_table.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_table_CellContentClick);
            this.DGV_table.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DGV_table_CellContentClick);
            // 
            // comboBox_table
            // 
            this.comboBox_table.FormattingEnabled = true;
            this.comboBox_table.Items.AddRange(new object[] {
            "java",
            "c_sharp",
            "c_plus_plus",
            "c",
            "python"});
            this.comboBox_table.Location = new System.Drawing.Point(972, 67);
            this.comboBox_table.Name = "comboBox_table";
            this.comboBox_table.Size = new System.Drawing.Size(175, 21);
            this.comboBox_table.TabIndex = 19;
            // 
            // btn_time_question
            // 
            this.btn_time_question.Location = new System.Drawing.Point(1026, 465);
            this.btn_time_question.Name = "btn_time_question";
            this.btn_time_question.Size = new System.Drawing.Size(121, 85);
            this.btn_time_question.TabIndex = 20;
            this.btn_time_question.Text = "Time and Question";
            this.btn_time_question.UseVisualStyleBackColor = true;
            this.btn_time_question.Click += new System.EventHandler(this.btn_time_question_Click);
            // 
            // Admin_Panel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1184, 562);
            this.Controls.Add(this.btn_time_question);
            this.Controls.Add(this.comboBox_table);
            this.Controls.Add(this.DGV_table);
            this.Controls.Add(this.btn_LOAD);
            this.Controls.Add(this.btn_DELETE);
            this.Controls.Add(this.btn_UPDATE);
            this.Controls.Add(this.btn_INSERT);
            this.Controls.Add(this.txt_Answer);
            this.Controls.Add(this.txt_D);
            this.Controls.Add(this.txt_C);
            this.Controls.Add(this.txt_B);
            this.Controls.Add(this.txt_A);
            this.Controls.Add(this.txt_question);
            this.Controls.Add(this.txt_ID);
            this.Controls.Add(this.lbl_Answer);
            this.Controls.Add(this.lbl_D);
            this.Controls.Add(this.lbl_C);
            this.Controls.Add(this.lbl_B);
            this.Controls.Add(this.lbl_A);
            this.Controls.Add(this.lbl_question);
            this.Controls.Add(this.lbl_ID);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(-500, 500);
            this.MaximumSize = new System.Drawing.Size(1200, 600);
            this.MinimumSize = new System.Drawing.Size(1200, 600);
            this.Name = "Admin_Panel";
            this.Text = "ADMIN PANEL";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Admin_Panel_FormClosed);
            this.Load += new System.EventHandler(this.Admin_Panel_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGV_table)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_ID;
        private System.Windows.Forms.Label lbl_question;
        private System.Windows.Forms.Label lbl_A;
        private System.Windows.Forms.Label lbl_B;
        private System.Windows.Forms.Label lbl_C;
        private System.Windows.Forms.Label lbl_D;
        private System.Windows.Forms.Label lbl_Answer;
        private System.Windows.Forms.TextBox txt_ID;
        private System.Windows.Forms.TextBox txt_question;
        private System.Windows.Forms.TextBox txt_A;
        private System.Windows.Forms.TextBox txt_B;
        private System.Windows.Forms.TextBox txt_C;
        private System.Windows.Forms.TextBox txt_D;
        private System.Windows.Forms.TextBox txt_Answer;
        private System.Windows.Forms.Button btn_INSERT;
        private System.Windows.Forms.Button btn_UPDATE;
        private System.Windows.Forms.Button btn_DELETE;
        private System.Windows.Forms.Button btn_LOAD;
        private System.Windows.Forms.DataGridView DGV_table;
        private System.Windows.Forms.ComboBox comboBox_table;
        private System.Windows.Forms.Button btn_time_question;
    }
}