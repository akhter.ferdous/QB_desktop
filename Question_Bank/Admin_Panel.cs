﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Windows.Forms;



namespace Question_Bank
{
    public partial class Admin_Panel : Form
    {
        Form pre;

        public Admin_Panel(Form p)
        {
            InitializeComponent();
            load_data();
           

            pre = p;
        }

        private void Admin_Panel_Load(object sender, EventArgs e)
        {

        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {

        }


        public void insert_data()
        {

            String MyConnectionString = "Server=localhost;Database=question_bank;Uid=root;pwd=";
            MySqlConnection connection = new MySqlConnection(MyConnectionString);

            connection.Open();

            String id = txt_ID.Text;
            String Question = txt_question.Text;
            String answer_a = txt_A.Text;
            String answer_b = txt_B.Text;
            String answer_c = txt_C.Text;
            String answer_d = txt_D.Text;
            String answer = txt_Answer.Text;


            String table_name = comboBox_table.Text;
            
            try
            {
               // String query = "insert into  " + table_name + "  (ID,Question,A,B,C,D,Answer) Values ('" + id + "','" + Question + "'," + answer_a + "," + answer_b + "," + answer_c + "," + answer_d + "," + answer + " );";
                String query = "insert into question_bank."+table_name+" Values ('" + id + "','" + Question + "','" + answer_a + "','" + answer_b + "','" + answer_c + "','" + answer_d + "','" + answer + "');";

                MySqlCommand cmd = new MySqlCommand(query,connection);
                MySqlDataReader reader = cmd.ExecuteReader();
                MessageBox.Show("SAVED");
               /* while (reader.Read())
                {

                }*/

                    


            }
            catch (Exception e)
            {

                MessageBox.Show("Select the table first\n\n" + e.Message);



            }

            finally
            {

                if (connection.State == ConnectionState.Open)
                {

                    connection.Close();

                }

            }

            load_data();


        }

        private void btn_INSERT_Click(object sender, EventArgs e)
        {

            insert_data();
            txt_ID.Text = "";
            txt_question.Text = "";
            txt_A.Text = "";
            txt_B.Text = "";
            txt_C.Text = "";
            txt_D.Text = "";
            txt_Answer.Text = "";
            


        }

        private void txt_question_TextChanged(object sender, EventArgs e)
        {
            
        }


        public void load_data()
        {

            String MyConnectionString = "Server=localhost;Database=question_bank;Uid=root;pwd=";
            MySqlConnection connection = new MySqlConnection(MyConnectionString);

            connection.Open();


            try
            {
                

                String table_name = comboBox_table.Text;
                if (table_name.Equals(""))
                {

                    table_name = "java";

                }
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM "+table_name;
               
                MySqlDataAdapter adap = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                
                adap.Fill(ds);
               

                DGV_table.DataSource = ds.Tables[0].DefaultView;


            }
            catch (Exception e)
            {

                MessageBox.Show("Select the table first\n\n" + e.Message);

            }

            finally
            {

                if (connection.State == ConnectionState.Open)
                {

                    connection.Close();

                }

            }


        }


        private void btn_LOAD_Click(object sender, EventArgs e)
        {
            load_data();
        }



        public void update_data()
        {

            String MyConnectionString = "Server=localhost;Database=question_bank;Uid=root;pwd=";
            MySqlConnection connection = new MySqlConnection(MyConnectionString);

            connection.Open();

            String id = txt_ID.Text;
            String Question = txt_question.Text;
            String answer_a = txt_A.Text;
            String answer_b = txt_B.Text;
            String answer_c = txt_C.Text;
            String answer_d = txt_D.Text;
            String answer = txt_Answer.Text;


            String table_name = comboBox_table.Text;

            try
            {
                String query = "UPDATE question_bank."+table_name+ " SET Question = '"+ Question+"', A = '"+answer_a+"', B = '"+answer_b+"', C = '"+answer_c+"', D = '"+answer_d+"', Answer = '"+answer+"' WHERE "+table_name+".ID = '"+id+"'";
                //String query = "insert into question_bank." + table_name + " Values ('" + id + "','" + Question + "','" + answer_a + "','" + answer_b + "','" + answer_c + "','" + answer_d + "','" + answer + "');";

                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader reader = cmd.ExecuteReader();
                MessageBox.Show("SAVED");
                /* while (reader.Read())
                 {

                 }*/




            }
            catch (Exception e)
            {

                MessageBox.Show("Select the table first\n\n"+e.Message);



            }

            finally
            {

                if (connection.State == ConnectionState.Open)
                {

                    connection.Close();

                }

            }

            load_data();



        }


        private void btn_UPDATE_Click(object sender, EventArgs e)
        {
            update_data();
            txt_ID.Text = "";
            txt_question.Text = "";
            txt_A.Text = "";
            txt_B.Text = "";
            txt_C.Text = "";
            txt_D.Text = "";
            txt_Answer.Text = "";
        }


        public void delete_data()
        {

            String MyConnectionString = "Server=localhost;Database=question_bank;Uid=root;pwd=";
            MySqlConnection connection = new MySqlConnection(MyConnectionString);

            connection.Open();

            String id = txt_ID.Text;
            


            String table_name = comboBox_table.Text;

            try
            {
                String query = "DELETE FROM question_bank."+table_name+" WHERE ID ='"+id+"'";
                //String query = "insert into question_bank." + table_name + " Values ('" + id + "','" + Question + "','" + answer_a + "','" + answer_b + "','" + answer_c + "','" + answer_d + "','" + answer + "');";

                MySqlCommand cmd = new MySqlCommand(query, connection);
                MySqlDataReader reader = cmd.ExecuteReader();
                MessageBox.Show("DELETED");
                /* while (reader.Read())
                 {

                 }*/




            }
            catch (Exception e)
            {

                MessageBox.Show("Select the table first\n\n" + e.Message);



            }

            finally
            {

                if (connection.State == ConnectionState.Open)
                {

                    connection.Close();

                }

            }

            load_data();


        }

        private void btn_DELETE_Click(object sender, EventArgs e)
        {
            delete_data();
            txt_ID.Text = "";
            txt_question.Text = "";
            txt_A.Text = "";
            txt_B.Text = "";
            txt_C.Text = "";
            txt_D.Text = "";
            txt_Answer.Text = "";
        }

        private void DGV_table_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            // MessageBox.Show(DGV_table.CurrentCell.FormattedValue.ToString());
            //MessageBox.Show(DGV_table.CurrentCell.Value.ToString());
            //MessageBox.Show(DGV_table.CurrentRow.Cells["ID"].Value.ToString());
            txt_ID.Text = DGV_table.CurrentRow.Cells["ID"].Value.ToString();
            txt_question.Text = DGV_table.CurrentRow.Cells["Question"].Value.ToString();
            txt_A.Text = DGV_table.CurrentRow.Cells["A"].Value.ToString();
            txt_B.Text = DGV_table.CurrentRow.Cells["B"].Value.ToString();
            txt_C.Text = DGV_table.CurrentRow.Cells["C"].Value.ToString();
            txt_D.Text = DGV_table.CurrentRow.Cells["D"].Value.ToString();
            txt_Answer.Text = DGV_table.CurrentRow.Cells["Answer"].Value.ToString(); 


        }

        private void DGV_table_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            // MessageBox.Show(DGV_table.CurrentCell.FormattedValue.ToString());
            //MessageBox.Show(DGV_table.CurrentCell.Value.ToString());
            //MessageBox.Show(DGV_table.CurrentRow.Cells["ID"].Value.ToString());
            txt_ID.Text = DGV_table.CurrentRow.Cells["ID"].Value.ToString();
            txt_question.Text = DGV_table.CurrentRow.Cells["Question"].Value.ToString();
            txt_A.Text = DGV_table.CurrentRow.Cells["A"].Value.ToString();
            txt_B.Text = DGV_table.CurrentRow.Cells["B"].Value.ToString();
            txt_C.Text = DGV_table.CurrentRow.Cells["C"].Value.ToString();
            txt_D.Text = DGV_table.CurrentRow.Cells["D"].Value.ToString();
            txt_Answer.Text = DGV_table.CurrentRow.Cells["Answer"].Value.ToString();


        }

        private void btn_time_question_Click(object sender, EventArgs e)
        {
            this.Hide();
            admin_time_question ob1 = new admin_time_question(this);

            ob1.Show();
        }

        private void Admin_Panel_FormClosed(object sender, FormClosedEventArgs e)
        {
            pre.Show();
        }
    }
}
