﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Question_Bank
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            String username = txt_username.Text;
            String password = txt_password.Text;

           // Admin_Panel ob1 = new Admin_Panel();
           // ob1.Show();
            if (username.Equals("admin") && password.Equals("admin"))
            {
                this.Hide();
                Admin_Panel ob1 = new Admin_Panel(this);
                ob1.Show();
                txt_password.Text = "";
                


            }

            else
            {
                MessageBox.Show("LOGIN FAILED");

            }
        }

        private void btn_question_Click(object sender, EventArgs e)
        {

            this.Hide();
            question_selection ob1 = new question_selection(this);
            ob1.Show();
        }

        private void txt_password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {

                btn_login_Click(this, new EventArgs());

            }
        }

        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            //DialogResult dialog = MessageBox.Show("Do you want to EXit the Application","Exit",MessageBoxButtons.YesNo);

            if (MessageBox.Show("Do you want to close the Application ??","EXIT",MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //Application.Exit();
                

            }
            else
            {
                e.Cancel = true;

            }

        }

        private void btn_about_Click(object sender, EventArgs e)
        {
            MessageBox.Show(" Question Bank\n Version 1.0\n Copyright © 2016, All rights reserved\n\n\n Akhter, Md. Ferdous\n 14-26478-2", "INFO",MessageBoxButtons.OK);
        }
    }
}
