﻿namespace Question_Bank
{
    partial class admin_time_question
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(admin_time_question));
            this.lbl_time_question_header = new System.Windows.Forms.Label();
            this.lbl_hour = new System.Windows.Forms.Label();
            this.lbl_min = new System.Windows.Forms.Label();
            this.lbl_sec = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txt_hour = new System.Windows.Forms.TextBox();
            this.txt_min = new System.Windows.Forms.TextBox();
            this.txt_sec = new System.Windows.Forms.TextBox();
            this.txt_question = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_questionstart = new System.Windows.Forms.TextBox();
            this.txt_questionend = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lbl_time_question_header
            // 
            this.lbl_time_question_header.AutoSize = true;
            this.lbl_time_question_header.BackColor = System.Drawing.Color.Transparent;
            this.lbl_time_question_header.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_time_question_header.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.lbl_time_question_header.Location = new System.Drawing.Point(29, 9);
            this.lbl_time_question_header.MaximumSize = new System.Drawing.Size(490, 29);
            this.lbl_time_question_header.MinimumSize = new System.Drawing.Size(490, 29);
            this.lbl_time_question_header.Name = "lbl_time_question_header";
            this.lbl_time_question_header.Size = new System.Drawing.Size(490, 29);
            this.lbl_time_question_header.TabIndex = 0;
            this.lbl_time_question_header.Text = "ENTER TIME AND QUESTION AMOUNT";
            // 
            // lbl_hour
            // 
            this.lbl_hour.AutoSize = true;
            this.lbl_hour.BackColor = System.Drawing.Color.Transparent;
            this.lbl_hour.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hour.Location = new System.Drawing.Point(56, 58);
            this.lbl_hour.Name = "lbl_hour";
            this.lbl_hour.Size = new System.Drawing.Size(65, 24);
            this.lbl_hour.TabIndex = 1;
            this.lbl_hour.Text = "HOUR";
            // 
            // lbl_min
            // 
            this.lbl_min.AutoSize = true;
            this.lbl_min.BackColor = System.Drawing.Color.Transparent;
            this.lbl_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_min.Location = new System.Drawing.Point(239, 58);
            this.lbl_min.Name = "lbl_min";
            this.lbl_min.Size = new System.Drawing.Size(92, 25);
            this.lbl_min.TabIndex = 2;
            this.lbl_min.Text = "MINUTE";
            // 
            // lbl_sec
            // 
            this.lbl_sec.AutoSize = true;
            this.lbl_sec.BackColor = System.Drawing.Color.Transparent;
            this.lbl_sec.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sec.Location = new System.Drawing.Point(409, 58);
            this.lbl_sec.Name = "lbl_sec";
            this.lbl_sec.Size = new System.Drawing.Size(101, 25);
            this.lbl_sec.TabIndex = 3;
            this.lbl_sec.Text = "SECOND";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(170, 94);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 55);
            this.label4.TabIndex = 4;
            this.label4.Text = ":";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(349, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 55);
            this.label5.TabIndex = 5;
            this.label5.Text = ":";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 175);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(196, 24);
            this.label6.TabIndex = 6;
            this.label6.Text = "QUESTION AMOUNT";
            // 
            // txt_hour
            // 
            this.txt_hour.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_hour.Location = new System.Drawing.Point(51, 94);
            this.txt_hour.Multiline = true;
            this.txt_hour.Name = "txt_hour";
            this.txt_hour.Size = new System.Drawing.Size(79, 68);
            this.txt_hour.TabIndex = 7;
            // 
            // txt_min
            // 
            this.txt_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_min.Location = new System.Drawing.Point(243, 94);
            this.txt_min.Multiline = true;
            this.txt_min.Name = "txt_min";
            this.txt_min.Size = new System.Drawing.Size(80, 68);
            this.txt_min.TabIndex = 8;
            this.txt_min.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // txt_sec
            // 
            this.txt_sec.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_sec.Location = new System.Drawing.Point(414, 94);
            this.txt_sec.Multiline = true;
            this.txt_sec.Name = "txt_sec";
            this.txt_sec.Size = new System.Drawing.Size(87, 68);
            this.txt_sec.TabIndex = 9;
            // 
            // txt_question
            // 
            this.txt_question.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_question.Location = new System.Drawing.Point(51, 214);
            this.txt_question.Multiline = true;
            this.txt_question.Name = "txt_question";
            this.txt_question.Size = new System.Drawing.Size(80, 65);
            this.txt_question.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(244, 348);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(79, 37);
            this.button1.TabIndex = 11;
            this.button1.Text = "APPLY";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(224, 175);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(124, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "Question start";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(395, 175);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(124, 24);
            this.label2.TabIndex = 13;
            this.label2.Text = "Question end";
            // 
            // txt_questionstart
            // 
            this.txt_questionstart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_questionstart.Location = new System.Drawing.Point(243, 214);
            this.txt_questionstart.Multiline = true;
            this.txt_questionstart.Name = "txt_questionstart";
            this.txt_questionstart.Size = new System.Drawing.Size(80, 65);
            this.txt_questionstart.TabIndex = 14;
            // 
            // txt_questionend
            // 
            this.txt_questionend.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_questionend.Location = new System.Drawing.Point(414, 214);
            this.txt_questionend.Multiline = true;
            this.txt_questionend.Name = "txt_questionend";
            this.txt_questionend.Size = new System.Drawing.Size(87, 65);
            this.txt_questionend.TabIndex = 15;
            // 
            // admin_time_question
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(559, 397);
            this.Controls.Add(this.txt_questionend);
            this.Controls.Add(this.txt_questionstart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txt_question);
            this.Controls.Add(this.txt_sec);
            this.Controls.Add(this.txt_min);
            this.Controls.Add(this.txt_hour);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_sec);
            this.Controls.Add(this.lbl_min);
            this.Controls.Add(this.lbl_hour);
            this.Controls.Add(this.lbl_time_question_header);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "admin_time_question";
            this.Text = "QUESTION AND TIME";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.admin_time_question_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbl_time_question_header;
        private System.Windows.Forms.Label lbl_hour;
        private System.Windows.Forms.Label lbl_min;
        private System.Windows.Forms.Label lbl_sec;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txt_hour;
        private System.Windows.Forms.TextBox txt_min;
        private System.Windows.Forms.TextBox txt_sec;
        private System.Windows.Forms.TextBox txt_question;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_questionstart;
        private System.Windows.Forms.TextBox txt_questionend;
    }
}