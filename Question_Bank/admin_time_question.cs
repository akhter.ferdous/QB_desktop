﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Question_Bank
{
    public partial class admin_time_question : Form
    {
        int h, m, s, q, question_s, question_e;
        Form pre;
        public admin_time_question(Form p)
        {
            InitializeComponent();
            pre = p;

            show();
        }

        public void update()
        {
            h = time_and_Q.get_hour();
            m = time_and_Q.get_min();
            s = time_and_Q.get_sec();
            q = time_and_Q.get_question_num();

            question_s = time_and_Q.get_question_start();
            question_e = time_and_Q.get_question_end();


        }

        public void show()
        {
            update();
            txt_hour.Text = Convert.ToString(h);
            txt_min.Text = Convert.ToString(m);
            txt_sec.Text = Convert.ToString(s);
            txt_question.Text = Convert.ToString(q);
            txt_questionstart.Text = Convert.ToString(question_s);
            txt_questionend.Text = Convert.ToString(question_e);
        }

        private void admin_time_question_FormClosed(object sender, FormClosedEventArgs e)
        {
            pre.Show();
        }

        public void input()
        {
            h = Convert.ToInt32(txt_hour.Text);
            m = Convert.ToInt32(txt_min.Text);
            s = Convert.ToInt32(txt_sec.Text);
            q = Convert.ToInt32(txt_question.Text);

            question_s = Convert.ToInt32(txt_questionstart.Text);
            question_e = Convert.ToInt32(txt_questionend.Text);

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            input();
            time_and_Q.set_hour(h);
            time_and_Q.set_min(m);
            time_and_Q.set_sec(s);
            time_and_Q.set_question_num(q);
            time_and_Q.set_question_start(question_s);
            time_and_Q.set_question_end(question_e);


            update();
            show();

            MessageBox.Show("Saved !!");
        }
    }
}
