﻿namespace Question_Bank
{
    partial class question_exam
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(question_exam));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btn_next = new System.Windows.Forms.Button();
            this.btn_pass = new System.Windows.Forms.Button();
            this.rb_d = new System.Windows.Forms.RadioButton();
            this.rb_b = new System.Windows.Forms.RadioButton();
            this.rb_c = new System.Windows.Forms.RadioButton();
            this.rb_a = new System.Windows.Forms.RadioButton();
            this.lbl_question = new System.Windows.Forms.Label();
            this.btn_submit = new System.Windows.Forms.Button();
            this.lbl_hour = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_min = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lbl_sec = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.btn_next);
            this.groupBox1.Controls.Add(this.btn_pass);
            this.groupBox1.Controls.Add(this.rb_d);
            this.groupBox1.Controls.Add(this.rb_b);
            this.groupBox1.Controls.Add(this.rb_c);
            this.groupBox1.Controls.Add(this.rb_a);
            this.groupBox1.Controls.Add(this.lbl_question);
            this.groupBox1.Location = new System.Drawing.Point(19, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(498, 398);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "QUESTION BOX";
            // 
            // btn_next
            // 
            this.btn_next.Location = new System.Drawing.Point(279, 343);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(122, 48);
            this.btn_next.TabIndex = 6;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = true;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // btn_pass
            // 
            this.btn_pass.Location = new System.Drawing.Point(16, 343);
            this.btn_pass.Name = "btn_pass";
            this.btn_pass.Size = new System.Drawing.Size(131, 48);
            this.btn_pass.TabIndex = 5;
            this.btn_pass.Text = "Pass";
            this.btn_pass.UseVisualStyleBackColor = true;
            this.btn_pass.Click += new System.EventHandler(this.btn_pass_Click);
            // 
            // rb_d
            // 
            this.rb_d.AutoSize = true;
            this.rb_d.Location = new System.Drawing.Point(279, 268);
            this.rb_d.Name = "rb_d";
            this.rb_d.Size = new System.Drawing.Size(68, 17);
            this.rb_d.TabIndex = 4;
            this.rb_d.Text = "Option_4";
            this.rb_d.UseVisualStyleBackColor = true;
            // 
            // rb_b
            // 
            this.rb_b.AutoSize = true;
            this.rb_b.Location = new System.Drawing.Point(279, 199);
            this.rb_b.Name = "rb_b";
            this.rb_b.Size = new System.Drawing.Size(68, 17);
            this.rb_b.TabIndex = 3;
            this.rb_b.Text = "Option_2";
            this.rb_b.UseVisualStyleBackColor = true;
            // 
            // rb_c
            // 
            this.rb_c.AutoSize = true;
            this.rb_c.Location = new System.Drawing.Point(16, 268);
            this.rb_c.Name = "rb_c";
            this.rb_c.Size = new System.Drawing.Size(68, 17);
            this.rb_c.TabIndex = 2;
            this.rb_c.Text = "Option_3";
            this.rb_c.UseVisualStyleBackColor = true;
            // 
            // rb_a
            // 
            this.rb_a.AutoSize = true;
            this.rb_a.CausesValidation = false;
            this.rb_a.Location = new System.Drawing.Point(16, 199);
            this.rb_a.Name = "rb_a";
            this.rb_a.Size = new System.Drawing.Size(68, 17);
            this.rb_a.TabIndex = 1;
            this.rb_a.Text = "Option_1";
            this.rb_a.UseVisualStyleBackColor = true;
            // 
            // lbl_question
            // 
            this.lbl_question.AutoSize = true;
            this.lbl_question.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_question.Location = new System.Drawing.Point(13, 20);
            this.lbl_question.MaximumSize = new System.Drawing.Size(450, 150);
            this.lbl_question.MinimumSize = new System.Drawing.Size(450, 150);
            this.lbl_question.Name = "lbl_question";
            this.lbl_question.Size = new System.Drawing.Size(450, 150);
            this.lbl_question.TabIndex = 0;
            this.lbl_question.Text = "Question";
            this.lbl_question.Click += new System.EventHandler(this.lbl_question_Click);
            // 
            // btn_submit
            // 
            this.btn_submit.Location = new System.Drawing.Point(536, 356);
            this.btn_submit.Name = "btn_submit";
            this.btn_submit.Size = new System.Drawing.Size(228, 48);
            this.btn_submit.TabIndex = 1;
            this.btn_submit.Text = "Submit";
            this.btn_submit.UseVisualStyleBackColor = true;
            this.btn_submit.Click += new System.EventHandler(this.btn_submit_Click);
            // 
            // lbl_hour
            // 
            this.lbl_hour.AutoSize = true;
            this.lbl_hour.BackColor = System.Drawing.Color.Transparent;
            this.lbl_hour.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_hour.ForeColor = System.Drawing.Color.Red;
            this.lbl_hour.Location = new System.Drawing.Point(532, 53);
            this.lbl_hour.Name = "lbl_hour";
            this.lbl_hour.Size = new System.Drawing.Size(49, 24);
            this.lbl_hour.TabIndex = 2;
            this.lbl_hour.Text = "hour";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(597, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(22, 31);
            this.label2.TabIndex = 3;
            this.label2.Text = ":";
            // 
            // lbl_min
            // 
            this.lbl_min.AutoSize = true;
            this.lbl_min.BackColor = System.Drawing.Color.Transparent;
            this.lbl_min.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_min.ForeColor = System.Drawing.Color.Red;
            this.lbl_min.Location = new System.Drawing.Point(635, 53);
            this.lbl_min.Name = "lbl_min";
            this.lbl_min.Size = new System.Drawing.Size(41, 24);
            this.lbl_min.TabIndex = 4;
            this.lbl_min.Text = "min";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(693, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(22, 31);
            this.label4.TabIndex = 5;
            this.label4.Text = ":";
            // 
            // lbl_sec
            // 
            this.lbl_sec.AutoSize = true;
            this.lbl_sec.BackColor = System.Drawing.Color.Transparent;
            this.lbl_sec.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_sec.ForeColor = System.Drawing.Color.Red;
            this.lbl_sec.Location = new System.Drawing.Point(724, 53);
            this.lbl_sec.Name = "lbl_sec";
            this.lbl_sec.Size = new System.Drawing.Size(40, 24);
            this.lbl_sec.TabIndex = 6;
            this.lbl_sec.Text = "sec";
            this.lbl_sec.Click += new System.EventHandler(this.lbl_sec_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // question_exam
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(776, 423);
            this.Controls.Add(this.lbl_sec);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lbl_min);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lbl_hour);
            this.Controls.Add(this.btn_submit);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "question_exam";
            this.Text = "QUIZ";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.question_exam_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.Button btn_pass;
        private System.Windows.Forms.RadioButton rb_d;
        private System.Windows.Forms.RadioButton rb_b;
        private System.Windows.Forms.RadioButton rb_c;
        private System.Windows.Forms.RadioButton rb_a;
        private System.Windows.Forms.Label lbl_question;
        private System.Windows.Forms.Button btn_submit;
        private System.Windows.Forms.Label lbl_hour;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_min;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbl_sec;
        private System.Windows.Forms.Timer timer1;
    }
}