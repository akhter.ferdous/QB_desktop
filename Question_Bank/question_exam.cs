﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using MySql.Data;
using MySql.Data.MySqlClient;


namespace Question_Bank

{
    public partial class question_exam : Form 
    {

        Form pre;
        String table_name, question_main, option_a, option_b, option_c, option_d, answer, id;

        String[] temp_answer = new string[time_and_Q.get_question_num()+1];
        int[] queue_index = new int[time_and_Q.get_question_num() + 1];

        int hour, minute, second,question_amount,rand_number,score;

       

        private void lbl_question_Click(object sender, EventArgs e)
        {

        }

        Queue st = new Queue();
       
        Random rnd = new Random();









        public question_exam(String t_name,Form p)
        {
            InitializeComponent();


            pre = p;


            table_name = t_name;

             hour = time_and_Q.get_hour();
             minute = time_and_Q.get_min();
             second = time_and_Q.get_sec(); 

          /*   hour = 0;
            minute = 0;
            second = 10; */

            question_amount = time_and_Q.get_question_num();

            for(int i = 0; i < question_amount; i++)
            {   
                check_point:

                rand_number = rnd.Next(time_and_Q.get_question_start(), time_and_Q.get_question_end());
                
                if (st.Contains(rand_number))
                {
                    //st.Push(rand_number);
                    goto check_point;

                }
                st.Enqueue(rand_number);
                
            }

            //MessageBox.Show(st.Count.ToString());

            st.CopyTo(queue_index,0);

            //show questions
            show_question();
            timer1.Start();
        }


        public void show_question()
        {

            

            String MyConnectionString = "Server=localhost;Database=question_bank;Uid=root;pwd=";
            MySqlConnection connection = new MySqlConnection(MyConnectionString);

            connection.Open();

            try
            {
                if (st.Count == 0)
                {
                    MessageBox.Show("Exam ends here... Press SUBMIT ");

                }
                else {
                    id = st.Dequeue().ToString();
                }

                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT * FROM "+table_name+" where id= "+id;

                //MySqlDataAdapter adap = new MySqlDataAdapter(cmd);

                MySqlDataReader dr = cmd.ExecuteReader();
                dr.Read();

                lbl_question.Text = dr.GetValue(1).ToString();
                rb_a.Text = dr.GetValue(2).ToString();
                rb_b.Text = dr.GetValue(3).ToString();
                rb_c.Text = dr.GetValue(4).ToString();
                rb_d.Text = dr.GetValue(5).ToString();
                answer = dr.GetValue(6).ToString();


                rb_a.Checked = false;
                rb_b.Checked = false;
                rb_c.Checked = false;
                rb_d.Checked = false;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {

                    connection.Close();

                }

            }



        }

        private void lbl_sec_Click(object sender, EventArgs e)
        {



        }


        


        private void btn_pass_Click(object sender, EventArgs e)
        {
            st.Enqueue(id);
            show_question();
            
        }

        private void btn_next_Click(object sender, EventArgs e)
        {
            try
            {
                if (rb_a.Checked == true || rb_b.Checked == true || rb_c.Checked == true || rb_d.Checked == true) {

                    // MessageBox.Show("Yout HCODE IS: "+queue_index.Contains(Int32.Parse(id)));
                    // MessageBox.Show("index is: " + Array.IndexOf(queue_index, Int32.Parse(id)));
                    if (rb_a.Checked)
                    {
                        temp_answer[Array.IndexOf(queue_index, Int32.Parse(id))] = rb_a.Text;
                    }
                    else if (rb_b.Checked)
                    {
                        temp_answer[Array.IndexOf(queue_index, Int32.Parse(id))] = rb_b.Text;
                    }
                    else if (rb_c.Checked)
                    {
                        temp_answer[Array.IndexOf(queue_index, Int32.Parse(id))] = rb_c.Text;
                    }
                    else if (rb_d.Checked)
                    {
                        temp_answer[Array.IndexOf(queue_index, Int32.Parse(id))] = rb_d.Text;
                    }


                    if (answer.Equals(temp_answer[Array.IndexOf(queue_index, Int32.Parse(id))]))
                    {
                        score++;
                        //MessageBox.Show(score.ToString());

                    }
                    
                    show_question();

                }

                else
                {
                    MessageBox.Show("Please select your answer !!");

                }

            }
            catch (Exception p)
            {
                MessageBox.Show(p.Message);

            }
            
        }


        private void btn_submit_Click(object sender, EventArgs e)
        {
            this.Hide();
            result_sheet ob1 = new result_sheet(score,ref temp_answer,ref queue_index,table_name,this);
            ob1.Show();
            btn_next.Enabled = false;
            btn_pass.Enabled = false;
            timer1.Stop();
        }


        private void  timer1_Tick(object sender, EventArgs e)
        {
            timer1.Interval = 1000;

            if (second>=1 && second <= 60)
            {
                second--;

            }
            else if (second == 0 && minute >= 1 && minute <=60)
            {
                minute--;
                second = 60;

            }
            else if (minute == 0 && hour >= 1 && hour <= 23)
            {

                hour--;
                minute = 59;
                second = 60;
            }

            if (second == 0 && minute == 0 && hour == 0)
            {

                timer1.Stop();
                btn_next.Enabled = false;
                btn_pass.Enabled = false;

                MessageBox.Show("TIMEs UP !!!! \nPlease ! Press SUBMIT to get the result.");

            }


            lbl_hour.Text = hour.ToString();
            lbl_min.Text = minute.ToString();
            lbl_sec.Text = second.ToString();

            

        }

        private void question_exam_FormClosed(object sender, FormClosedEventArgs e)
        {
            pre.Show();
        }

    }
}
