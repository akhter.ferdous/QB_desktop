﻿namespace Question_Bank
{
    partial class question_selection
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(question_selection));
            this.btn_c = new System.Windows.Forms.Button();
            this.btn_c_plus = new System.Windows.Forms.Button();
            this.btn_c_sharp = new System.Windows.Forms.Button();
            this.btn_java = new System.Windows.Forms.Button();
            this.btn_python = new System.Windows.Forms.Button();
            this.lable1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_c
            // 
            this.btn_c.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_c.Location = new System.Drawing.Point(80, 74);
            this.btn_c.Name = "btn_c";
            this.btn_c.Size = new System.Drawing.Size(127, 84);
            this.btn_c.TabIndex = 0;
            this.btn_c.Text = "C";
            this.btn_c.UseVisualStyleBackColor = true;
            this.btn_c.Click += new System.EventHandler(this.btn_c_Click);
            // 
            // btn_c_plus
            // 
            this.btn_c_plus.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_c_plus.Location = new System.Drawing.Point(330, 74);
            this.btn_c_plus.Name = "btn_c_plus";
            this.btn_c_plus.Size = new System.Drawing.Size(125, 84);
            this.btn_c_plus.TabIndex = 1;
            this.btn_c_plus.Text = "C++";
            this.btn_c_plus.UseVisualStyleBackColor = true;
            this.btn_c_plus.Click += new System.EventHandler(this.btn_c_plus_Click);
            // 
            // btn_c_sharp
            // 
            this.btn_c_sharp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_c_sharp.Location = new System.Drawing.Point(80, 186);
            this.btn_c_sharp.Name = "btn_c_sharp";
            this.btn_c_sharp.Size = new System.Drawing.Size(127, 84);
            this.btn_c_sharp.TabIndex = 2;
            this.btn_c_sharp.Text = "C#";
            this.btn_c_sharp.UseVisualStyleBackColor = true;
            this.btn_c_sharp.Click += new System.EventHandler(this.btn_c_sharp_Click);
            // 
            // btn_java
            // 
            this.btn_java.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_java.Location = new System.Drawing.Point(330, 186);
            this.btn_java.Name = "btn_java";
            this.btn_java.Size = new System.Drawing.Size(125, 84);
            this.btn_java.TabIndex = 3;
            this.btn_java.Text = "JAVA";
            this.btn_java.UseVisualStyleBackColor = true;
            this.btn_java.Click += new System.EventHandler(this.btn_java_Click);
            // 
            // btn_python
            // 
            this.btn_python.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_python.Location = new System.Drawing.Point(175, 296);
            this.btn_python.Name = "btn_python";
            this.btn_python.Size = new System.Drawing.Size(197, 84);
            this.btn_python.TabIndex = 4;
            this.btn_python.Text = "PYTHON";
            this.btn_python.UseVisualStyleBackColor = true;
            this.btn_python.Click += new System.EventHandler(this.btn_python_Click);
            // 
            // lable1
            // 
            this.lable1.AutoSize = true;
            this.lable1.BackColor = System.Drawing.Color.Transparent;
            this.lable1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lable1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lable1.Location = new System.Drawing.Point(54, 9);
            this.lable1.Name = "lable1";
            this.lable1.Size = new System.Drawing.Size(425, 37);
            this.lable1.TabIndex = 5;
            this.lable1.Text = "SELECT YOUR PLATFORM";
            // 
            // question_selection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(539, 412);
            this.Controls.Add(this.lable1);
            this.Controls.Add(this.btn_python);
            this.Controls.Add(this.btn_java);
            this.Controls.Add(this.btn_c_sharp);
            this.Controls.Add(this.btn_c_plus);
            this.Controls.Add(this.btn_c);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(555, 450);
            this.MinimumSize = new System.Drawing.Size(555, 450);
            this.Name = "question_selection";
            this.Text = "PLATFORMS";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.question_selection_FormClosed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_c;
        private System.Windows.Forms.Button btn_c_plus;
        private System.Windows.Forms.Button btn_c_sharp;
        private System.Windows.Forms.Button btn_java;
        private System.Windows.Forms.Button btn_python;
        private System.Windows.Forms.Label lable1;
    }
}