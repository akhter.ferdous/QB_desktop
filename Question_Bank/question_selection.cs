﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Question_Bank
{
    
    public partial class question_selection : Form
    {

        Form pre;
        public question_selection(Form p)
        {
            InitializeComponent();
            pre = p;
        }

        private void btn_python_Click(object sender, EventArgs e)
        {
            this.Hide();
            question_exam ob5 = new question_exam("python",this);
            ob5.Show();
        }

        private void btn_c_Click(object sender, EventArgs e)
        {
            this.Hide();
            question_exam ob1 = new question_exam("c",this);
            ob1.Show();
        }

        private void btn_c_plus_Click(object sender, EventArgs e)
        {
            this.Hide();
            question_exam ob2 = new question_exam("c_plus_plus",this);
            ob2.Show();
        }

        private void btn_c_sharp_Click(object sender, EventArgs e)
        {
            this.Hide();
            question_exam ob3 = new question_exam("c_sharp",this);
            ob3.Show();
        }

        private void btn_java_Click(object sender, EventArgs e)
        {
            this.Hide();
            question_exam ob4 = new question_exam("java",this);
            ob4.Show();
        }

        private void question_selection_FormClosed(object sender, FormClosedEventArgs e)
        {
            pre.Show();
        }
    }
}
