﻿namespace Question_Bank
{
    partial class result_sheet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(result_sheet));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txt_rightanswer = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_next = new System.Windows.Forms.Button();
            this.rb_d = new System.Windows.Forms.RadioButton();
            this.rb_b = new System.Windows.Forms.RadioButton();
            this.rb_c = new System.Windows.Forms.RadioButton();
            this.rb_a = new System.Windows.Forms.RadioButton();
            this.lbl_question = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_studentanswer = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl_score = new System.Windows.Forms.Label();
            this.btn_exit = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txt_rightanswer);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.btn_next);
            this.groupBox1.Controls.Add(this.rb_d);
            this.groupBox1.Controls.Add(this.rb_b);
            this.groupBox1.Controls.Add(this.rb_c);
            this.groupBox1.Controls.Add(this.rb_a);
            this.groupBox1.Controls.Add(this.lbl_question);
            this.groupBox1.Location = new System.Drawing.Point(12, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(498, 398);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "QUESTION BOX";
            // 
            // txt_rightanswer
            // 
            this.txt_rightanswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_rightanswer.Location = new System.Drawing.Point(175, 335);
            this.txt_rightanswer.Multiline = true;
            this.txt_rightanswer.Name = "txt_rightanswer";
            this.txt_rightanswer.Size = new System.Drawing.Size(144, 51);
            this.txt_rightanswer.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(15, 348);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(134, 24);
            this.label3.TabIndex = 7;
            this.label3.Text = "Right Answer";
            // 
            // btn_next
            // 
            this.btn_next.Location = new System.Drawing.Point(370, 339);
            this.btn_next.Name = "btn_next";
            this.btn_next.Size = new System.Drawing.Size(122, 48);
            this.btn_next.TabIndex = 6;
            this.btn_next.Text = "Next";
            this.btn_next.UseVisualStyleBackColor = true;
            this.btn_next.Click += new System.EventHandler(this.btn_next_Click);
            // 
            // rb_d
            // 
            this.rb_d.AutoSize = true;
            this.rb_d.Location = new System.Drawing.Point(279, 268);
            this.rb_d.Name = "rb_d";
            this.rb_d.Size = new System.Drawing.Size(68, 17);
            this.rb_d.TabIndex = 4;
            this.rb_d.TabStop = true;
            this.rb_d.Text = "Option_4";
            this.rb_d.UseVisualStyleBackColor = true;
            // 
            // rb_b
            // 
            this.rb_b.AutoSize = true;
            this.rb_b.Location = new System.Drawing.Point(279, 199);
            this.rb_b.Name = "rb_b";
            this.rb_b.Size = new System.Drawing.Size(68, 17);
            this.rb_b.TabIndex = 3;
            this.rb_b.TabStop = true;
            this.rb_b.Text = "Option_2";
            this.rb_b.UseVisualStyleBackColor = true;
            // 
            // rb_c
            // 
            this.rb_c.AutoSize = true;
            this.rb_c.Location = new System.Drawing.Point(16, 268);
            this.rb_c.Name = "rb_c";
            this.rb_c.Size = new System.Drawing.Size(68, 17);
            this.rb_c.TabIndex = 2;
            this.rb_c.TabStop = true;
            this.rb_c.Text = "Option_3";
            this.rb_c.UseVisualStyleBackColor = true;
            // 
            // rb_a
            // 
            this.rb_a.AutoSize = true;
            this.rb_a.Location = new System.Drawing.Point(16, 199);
            this.rb_a.Name = "rb_a";
            this.rb_a.Size = new System.Drawing.Size(68, 17);
            this.rb_a.TabIndex = 1;
            this.rb_a.TabStop = true;
            this.rb_a.Text = "Option_1";
            this.rb_a.UseVisualStyleBackColor = true;
            // 
            // lbl_question
            // 
            this.lbl_question.AutoSize = true;
            this.lbl_question.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_question.Location = new System.Drawing.Point(13, 20);
            this.lbl_question.Name = "lbl_question";
            this.lbl_question.Size = new System.Drawing.Size(61, 16);
            this.lbl_question.TabIndex = 0;
            this.lbl_question.Text = "Question";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Location = new System.Drawing.Point(524, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(213, 37);
            this.label1.TabIndex = 2;
            this.label1.Text = "Your Answer";
            // 
            // txt_studentanswer
            // 
            this.txt_studentanswer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_studentanswer.Location = new System.Drawing.Point(530, 78);
            this.txt_studentanswer.Multiline = true;
            this.txt_studentanswer.Name = "txt_studentanswer";
            this.txt_studentanswer.Size = new System.Drawing.Size(207, 140);
            this.txt_studentanswer.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(569, 236);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 37);
            this.label2.TabIndex = 4;
            this.label2.Text = "SCORE";
            // 
            // lbl_score
            // 
            this.lbl_score.AutoSize = true;
            this.lbl_score.BackColor = System.Drawing.Color.Transparent;
            this.lbl_score.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl_score.Location = new System.Drawing.Point(595, 303);
            this.lbl_score.Name = "lbl_score";
            this.lbl_score.Size = new System.Drawing.Size(108, 37);
            this.lbl_score.TabIndex = 5;
            this.lbl_score.Text = "label3";
            // 
            // btn_exit
            // 
            this.btn_exit.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_exit.ForeColor = System.Drawing.Color.Red;
            this.btn_exit.Location = new System.Drawing.Point(609, 370);
            this.btn_exit.Name = "btn_exit";
            this.btn_exit.Size = new System.Drawing.Size(127, 50);
            this.btn_exit.TabIndex = 6;
            this.btn_exit.Text = "EXIT";
            this.btn_exit.UseVisualStyleBackColor = true;
            this.btn_exit.Click += new System.EventHandler(this.EXIT_Click);
            // 
            // result_sheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(769, 424);
            this.Controls.Add(this.btn_exit);
            this.Controls.Add(this.lbl_score);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txt_studentanswer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Name = "result_sheet";
            this.Text = "RESULT";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.result_sheet_FormClosed);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btn_next;
        private System.Windows.Forms.RadioButton rb_d;
        private System.Windows.Forms.RadioButton rb_b;
        private System.Windows.Forms.RadioButton rb_c;
        private System.Windows.Forms.RadioButton rb_a;
        private System.Windows.Forms.Label lbl_question;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_studentanswer;
        private System.Windows.Forms.TextBox txt_rightanswer;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbl_score;
        private System.Windows.Forms.Button btn_exit;
    }
}