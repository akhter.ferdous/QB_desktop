﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using MySql.Data;
using MySql.Data.MySqlClient;

namespace Question_Bank
{
   
    public partial class result_sheet : Form
    {
        Form pre;

        int question_number = time_and_Q.get_question_num();
        int counter;

        String table_name;
        int[] queue_index = new int[time_and_Q.get_question_num()];

        String[] temp_answer = new String[time_and_Q.get_question_num()];



        public result_sheet(int score,ref String[] t_answer,ref int[] q_index,String t_name,Form p)
        {
            InitializeComponent();



            pre = p;

            lbl_score.Text = score.ToString();
            table_name = t_name;
            queue_index = q_index;
            temp_answer = t_answer;


            show_answers();

        }

        public void show_answers()
        {



            String MyConnectionString = "Server=localhost;Database=question_bank;Uid=root;pwd=";
            MySqlConnection connection = new MySqlConnection(MyConnectionString);

            connection.Open();

            try
            {
                if (counter < question_number)
                {
                    txt_rightanswer.BackColor = Color.White;

                    MySqlCommand cmd = connection.CreateCommand();
                    cmd.CommandText = "SELECT * FROM " + table_name + " where id= " + queue_index[counter];

                    //MySqlDataAdapter adap = new MySqlDataAdapter(cmd);

                    MySqlDataReader dr = cmd.ExecuteReader();
                    dr.Read();

                    lbl_question.Text = dr.GetValue(1).ToString();
                    rb_a.Text = dr.GetValue(2).ToString();
                    rb_a.Checked = false;

                    rb_b.Text = dr.GetValue(3).ToString();
                    rb_b.Checked = false;

                    rb_c.Text = dr.GetValue(4).ToString();
                    rb_c.Checked = false;

                    rb_d.Text = dr.GetValue(5).ToString();
                    rb_d.Checked = false;

                    txt_rightanswer.Text = dr.GetValue(6).ToString();

                    txt_studentanswer.Text = temp_answer[counter];

                    if (txt_rightanswer.Text.Equals(txt_studentanswer.Text))
                    {

                        txt_rightanswer.BackColor=Color.Green;

                    }
                    else
                    {

                        txt_rightanswer.BackColor = Color.Red;
                    }


                    counter++;
                }
                else
                {

                    MessageBox.Show("ALL THE answers are already shown !!");
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {

                    connection.Close();

                }

            }




        }




        private void btn_next_Click(object sender, EventArgs e)
        {
            show_answers();
        }

        private void result_sheet_FormClosed(object sender, FormClosedEventArgs e)
        {
            pre.Show();
        }

        private void EXIT_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
