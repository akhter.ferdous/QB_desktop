﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Question_Bank
{
    static class time_and_Q
    {
        public static int hour;
        public static int min;
        public static int sec;

        public static int question_num;

        public static int start_question_no, end_question_no;


        static time_and_Q()
        {
            hour = 0;
            min = 20;
            sec = 59;

            question_num = 20;

            start_question_no = 1;
            end_question_no = 100;

        } 

        public static void set_hour(int item)
        {
            if (item >= 0 && item <=24) {
                hour = item;
                }
            else
            {
                MessageBox.Show("check hour input");

            }
          }

        public static void set_min(int item)
        {

            if (item >= 0 && item <= 59)
            {
                min = item;
            }
            else
            {
                MessageBox.Show("check min input");

            }
        }

        public static void set_sec(int item)
        {
            if (item >= 0 && item <= 59)
            {
                sec = item;
            }
            else
            {
                MessageBox.Show("check sec input");

            }

        }

        public static void set_question_num(int item)
        {
            question_num = item;

        }


        public static int get_question_num()
        {
            return question_num;

        }
        public static int get_hour()
        {
            return hour;

        }

        public static int get_sec()
        {
            return sec;

        }

        public static int get_min()
        {
            return min;

        }


        public static void set_question_start(int p)
        {
            start_question_no = p;

        }

        public static void set_question_end(int p)
        {
            end_question_no = p;

        }

        public static int get_question_start()
        {
            return start_question_no;

        }

        public static int get_question_end()
        {
            return end_question_no;

        }

    }
}
