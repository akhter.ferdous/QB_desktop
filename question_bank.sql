-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Sep 05, 2016 at 04:48 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `question_bank`
--

-- --------------------------------------------------------

--
-- Table structure for table `c`
--

CREATE TABLE `c` (
  `ID` int(11) NOT NULL,
  `Question` varchar(300) NOT NULL,
  `A` varchar(50) NOT NULL,
  `B` varchar(50) NOT NULL,
  `C` varchar(50) NOT NULL,
  `D` varchar(50) NOT NULL,
  `Answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c`
--

INSERT INTO `c` (`ID`, `Question`, `A`, `B`, `C`, `D`, `Answer`) VALUES
(1, 'Power spectral density function is a ?\r\n', 'Real and even function', 'Non negative function', 'Periodic', 'All of the mentioned', 'All of the mentioned'),
(2, ' Energy spectral density defines\r\n', 'Signal energy per unit area', 'Signal energy per unit bandwidth', 'Signal power per unit area', 'Signal power per unit bandwidth', 'Signal energy per unit bandwidth'),
(3, 'Power spectrum describes distribution of _________ under frequency domain.\r\n', 'Mean', 'Variance', 'Gaussian', 'None of the mentioned', 'Variance'),
(4, 'How can power spectral density of non periodic signal be calculated?\r\n', 'By integrating', 'By truncating', 'By converting to periodic', ' None of the mentioned', 'By truncating'),
(5, 'What is Wiener-Khinchin theorem?\r\n ', 'Spectral density and auto-covariance makes a fouri', 'Spectral density and auto-correlatioon makes a fou', 'Spectral density and variance makes a fourier tran', 'None of the mentioned', 'Spectral density and auto-correlatioon makes a fou'),
(6, 'According to Parseval’s theorem the energy spectral density curve is equal to?\r\n', 'Area under magnitude of the signal', 'Area under square of the magnitude of the signal', ' Area under square root of magnitude of the signal', 'None of the mentioned', 'Area under square of the magnitude of the signal'),
(7, 'Spectogram is the graph plotted against ?\r\n', 'Frequency domain', 'Time domain', 'Both of the mentioned', 'None of the mentioned', 'Time domain'),
(8, 'Autocorrelation is a function which matches –\r\n', 'Two same signals', 'Two different signal', 'One signal with its delayed version', 'None of the mentioned', 'One signal with its delayed version'),
(9, ' Autocorrelation is a function of\r\n', 'Time', 'Frequency', 'Time difference', 'Frequency difference ', 'Time difference'),
(10, 'Autocorrelation is maximum at _______\r\n', 'Unity', 'Origin', 'Infinite point', 'None of the mentioned', 'Origin'),
(11, 'Autocorrelation function of periodic signal is equal to _______\r\n', 'Energy of the signal', 'Power of the signal', 'Its area in frequency domain', 'None of the mentioned', 'Power of the signal'),
(12, 'Autocorrelation is a _______ function.\r\n ', 'Real and even', 'Real and odd', 'Complex and even', 'Complex and odd', 'Real and even'),
(13, 'Autocorrelation function of white noise will have ?\r\n', 'Strong peak', 'Infinite peak', 'Weak peak', ' None of the mentioned', 'Strong peak'),
(14, 'What is the period of a signal x(t) ?\r\n', 'T', '2T', 'T/2', 'None of the mentioned', 'T'),
(15, 'Which of the given signals are periodic?\r\n', 'x(t) = 4 cos(5pt)', 'x(t) = u(t) – 1/2', ' x(t) = 4u(t) + 2sin(3t)', 'x[n] = 2sin(3n)', 'x(t) = 4 cos(5pt)'),
(16, ' Check whether the signal is periodic or not?\r\nx(t) = cos(4t) + 2sin(8t)\r\n\r\n ', 'Periodic with period p/2', 'Periodic with period 2', ' Periodic with period 2/p', 'Not periodic', 'Periodic with period p/2'),
(17, ' Find the periodicity of the following signal. x(t)=cos((2p/7)t)sin((3p/5)t)\r\n', '30', '7', '35', '5/3', '35'),
(18, 'Find the fundamental period of 1+sin^2 (3p/5)n .\r\n', '10/3', '5', '3p/5', 'None of the mentioned', '5'),
(19, 'Which signal is called as energy signal?\r\n ', 'Finite energy and zero power', 'Finite energy and non-zero power', 'Infinite energy and zero power', 'Infinite energy and non-zero power', 'Finite energy and zero power'),
(20, ' Which signal is said to be power signal?\r\n', 'Infinite power and zero energy', 'Infinite power and non-zero energy', ' Finite power and infinite energy', 'Finite power and zero energy', ' Finite power and infinite energy'),
(21, 'Continuous Impulse signal is a power or energy signal?\r\n', 'Power signal', ' Energy signal', 'Both power and energy', 'Neither power nor energy signal', 'Neither power nor energy signal'),
(22, ' Discrete impulse signal is a power or energy signal?\r\n', 'Power signal', 'Energy signal', 'Both power and energy signal', 'Neither power or energy signal', 'Energy signal'),
(23, 'An unit impulse function has ?\r\n', 'Large amplitude', 'Zero pulse width', 'Unity weight', 'All of the mentioned', 'All of the mentioned'),
(24, 'Which circuit is called as regenerative repeaters?\r\n', 'Analog circuits', 'Digital circuits', 'Amplifiers', 'A/D converters', 'Digital circuits'),
(25, 'What are the advantages of digital circuits?\r\n', 'Less noise', ' Less interference', 'More flexible', 'All of the mentioned', 'All of the mentioned');

-- --------------------------------------------------------

--
-- Table structure for table `c_plus_plus`
--

CREATE TABLE `c_plus_plus` (
  `ID` int(11) NOT NULL,
  `Question` varchar(300) NOT NULL,
  `A` varchar(50) NOT NULL,
  `B` varchar(50) NOT NULL,
  `C` varchar(50) NOT NULL,
  `D` varchar(50) NOT NULL,
  `Answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c_plus_plus`
--

INSERT INTO `c_plus_plus` (`ID`, `Question`, `A`, `B`, `C`, `D`, `Answer`) VALUES
(1, 'How many characters are specified in the ASCII scheme?\r\n', ' 64', '128', '256', 'none of the mentioned', '128'),
(2, ' Select the right option.\r\nGiven the variables p, q are of char type and r, s, t are of int type\r\n1. t = (r * s) / (r + s);\r\n2. t = (p * q) / (r + s);\r\n', '1 is true but 2 is false', '1 is false and 2 is true', 'both 1 and 2 are true', 'both 1 and 2 are false', 'both 1 and 2 are true'),
(3, ' Which of the following belongs to the set of character types?\r\n', 'char', ' wchar_t', 'only a', 'both a and b', 'both a and b'),
(4, 'What will be the output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        char c = 74;\r\n        cout << c;\r\n        return 0;\r\n    }\r\n', 'A', 'N', ' J', 'I', ' J'),
(5, ' How do we represent a wide character of the form wchar_t?\r\n', ' L’a’', ' l’a’', 'L[a]', 'la', ' L’a’'),
(6, 'In C++, what is the sign of character data type by default?\r\n', 'Signed', 'Unsigned', 'Implementation dependent', 'None of these', 'Implementation dependent'),
(7, ' Is the size of character literals different in C and C++?\r\n', 'Implementation defined', 'Can’t say', 'Yes, they are different', 'No, they are not different', ''),
(8, 'Suppose in a hypothetical machine, the size of char is 32 bits. What would sizeof(char) return?\r\n', '4', '1', ' Implementation dependent', 'Machine dependent', '1'),
(9, ' What constant defined in <climits> header returns the number of bits in a char?\r\n', 'CHAR_SIZE', ' SIZE_CHAR', 'BIT_CHAR', 'CHAR_BIT', 'CHAR_BIT'),
(10, ' Is bool a fundamental datatype in C++?\r\n ', 'Yes', 'No, it is a typedef of unsigned char', 'No, it is an enum of {false,true}', 'No, it is expanded from macros', 'Yes'),
(11, ' Find the odd one out:\r\n', 'std::vector<int>', 'std::vector<short>', 'std::vector<long>', 'std::vector<bool>', ''),
(12, ' What is the value of the bool?\r\n\r\nbool is_int(789.54)\r\n', 'True', ' False', '1', 'none of the mentioned', ' False'),
(13, 'What happens when a null pointer is converted into bool?\r\n', ' An error is flagged', 'bool value evaluates to true', 'bool value evaluates to false', ' the statement is ignored', 'bool value evaluates to false'),
(14, 'Which of the following statements are false?\r\n', 'bool can have two values and can be used to expres', 'bool cannot be used as the type of the result of t', 'bool can be converted into integers implicitly', 'a bool value can be used in arithemetic expression', 'bool cannot be used as the type of the result of t'),
(15, ' For what values of the expression is an if-statement block not executed?\r\n', '0 and all negative values', ' 0 and -1', '0', '0, all negative values, all positive values except', '0'),
(16, 'Which of the two operators ++ and — work for the bool datatype in C++?\r\n', 'None', '++', '—', 'Both', ''),
(17, 'What is the output of the following program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int f(int p, int q)\r\n    {\r\n        if (p > q)\r\n            return p;\r\n        else\r\n            return q;\r\n    }\r\n    main()\r\n    {\r\n        int a = 5, b = 10;\r\n        int k;\r\n        bool x = tr', '55', '62', '52', 'none of the mentioned', '52'),
(18, 'What is the value of p?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        int p;\r\n        bool a = true;\r\n        bool b = false;\r\n        int x = 10;\r\n        int y = 5;\r\n        p = ((x | y) + (a + b));\r\n        cout << p;\r\n        return 0;\r\n    }\r\n', '0', '16', '12', '2', '16'),
(19, 'Evaluate the following\r\n(false && true) || false || true\r\n', ' 0', '1', 'false', 'none of the mentioned', '1'),
(20, 'What is the size of wchar_t in C++?\r\n', '2', '4', '2 or 4', ' based on the number of bits in the system', ' based on the number of bits in the system'),
(21, 'Pick the odd one out\r\n', 'array type', 'character type', 'boolean type', 'integer type', 'array type'),
(22, 'Which datatype is used to represent the absence of parameters?\r\n ', 'int', 'short', 'void', 'float', 'void'),
(23, ' What does a escape code represent?\r\n', 'alert', 'backslash', 'tab', 'form feed', 'alert'),
(24, 'Which type is best suited to represent the logical values?\r\n', 'integer', 'boolean', 'character', 'all of the mentioned', 'boolean'),
(25, ' Identify the user-defined types from the following?\r\n', 'enumeration', 'classes', 'both a and b', 'int', 'both a and b'),
(26, 'Which of the following statements are true?\r\n    int f(float)\r\n', ' f is a function taking an argument of type int an', 'f is a function taking an argument of type float a', 'f is a function of type float', 'none of the mentioned', 'f is a function taking an argument of type float a'),
(27, ' The value 132.54 can represented using which data type?\r\n', 'double', 'void', ' int', ' bool', 'double'),
(28, 'When a language has the capability to produce new data type mean, it can be called as\r\n', 'overloaded', 'extensible', 'encapsulated', ' reprehensible', 'extensible'),
(29, 'Pick the odd one out.\r\n', ' integer, character, boolean, floating', ' enumeration, classes', 'integer, enum, void', 'arrays, pointer, classes', 'integer, enum, void'),
(30, 'The size of an object or a type can be determined using which operator?\r\n', 'malloc', 'sizeof', 'malloc', 'calloc', 'sizeof'),
(31, 'It is guaranteed that a ____ has atleast 8bits and a ____ has atleast 16 bits.\r\n', ' int, float', 'char, int', 'bool, char', 'char, short', 'char, short'),
(32, 'Implementation dependent aspects about an implementation can be found in ____\r\n', '<implementation>', '<limits>', '<limit>', '<numeric>', '<limits>'),
(33, 'Size of C++ objects are expressed in terms of multiples of the size of a ____ and the size of a char is ____.\r\n', 'char, 1', ' int, 1', 'float, 8', 'char, 4', 'char, 1'),
(34, 'What is the output of the following program?\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main ( )\r\n    {\r\n        static double i;\r\n        i = 20;\r\n        cout << sizeof(i);\r\n        return 0;\r\n    }\r\n', '4', '2', '8', 'garbage', '8'),
(35, 'What is the output of the following program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        int num1 = 10;\r\n        float num2 = 20;\r\n        cout << sizeof(num1 + num2);\r\n        return 0;\r\n    }\r\n', '2', '4', '8', 'garbage', '4'),
(36, 'What would be the output of the following program (in 32-bit systems)?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        cout << sizeof(char);\r\n        cout << sizeof(int);\r\n        cout << sizeof(float);\r\n        return 0;\r\n    }\r\n', '1 4 4', '1 4 8', '1 8 8', 'none of the mentioned', '1 4 4'),
(37, 'Which of the following is not one of the sizes of the floating point types?\r\n', ' short float', ' float', 'long double', 'double', ' short float'),
(38, 'Which of the following is a valid floating point literal?\r\n', 'f287.333', 'F287.333', '287.e2', '287.3.e2', '287.e2'),
(39, 'What is the range of the floating point numbers?\r\n', '-3.4E+38 to +3.4E+38', ' -3.4E+38 to +3.4E+34', ' -3.4E+38 to +3.4E+36', '-3.4E+38 to +3.4E+32', '-3.4E+38 to +3.4E+38'),
(40, 'Which of three sizes of floating point types should be used when extended precision is required?\r\n', 'float', 'double', 'long double', 'extended float', 'long double'),
(41, 'What is the output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        float num1 = 1.1;\r\n        double num2 = 1.1;\r\n        if (num1 == num2)\r\n           cout << "stanford";\r\n        else\r\n           cout << "harvard";\r\n        return 0;\r\n    }\r\n', 'harvard', 'stanford', 'compile time error', 'runtime error', 'harvard'),
(42, ' What is the output of this program?\r\n\r\n    #include <iomanip>\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        cout << setprecision(17);\r\n        double d = 0.1;\r\n        cout << d << endl;\r\n        return 0;\r\n    }\r\n', '0.11', '0.10000000000000001', '0.100001', 'compile time error', '0.10000000000000001'),
(43, 'What is the output of the following program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        float i = 123.0f;\r\n        cout << i << endl;\r\n        return 0;\r\n    }\r\n', '123.00', '1.23', '123', 'compile time error', '123'),
(44, ' Which is used to indicate single precision value?\r\n', 'F or f', 'L or l', 'either a or b', 'neither a or b', 'F or f'),
(45, 'What is the output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        float f1 = 0.5;\r\n        double f2 = 0.5;\r\n        if (f1 == 0.5f)\r\n            cout << "equal";\r\n        else\r\n            cout << "not equal";\r\n        return 0;\r\n    }\r\n', 'equal', 'not equal', 'compile time error', 'runtime erro', 'equal'),
(46, ' The size_t integer type in C++ is?\r\n', 'Unsigned integer of at least 64 bits', 'Signed integer of at least 16 bits', 'Unsigned integer of at least 16 bits', 'Signed integer of at least 64 bits', 'Unsigned integer of at least 16 bits'),
(47, 'What is the output of the following program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n    	int x = -1;\r\n        unsigned int y = 2;\r\n \r\n        if(x > y) {\r\n        	cout << "x is greater";\r\n    	} else {\r\n    		cout << "y is greater";\r\n    	}\r\n    }\r\n', 'x is greater', 'y is greater', 'Implementation defined', 'Arbitrary', 'x is greater'),
(48, 'Which of these expressions will return true if the input integer v is a power of two?\r\n ', ' (v | (v + 1)) == 0;', '(~v & (v – 1)) == 0;', '(v | (v – 1)) == 0;', '(v & (v – 1)) == 0;', '(v & (v – 1)) == 0;'),
(49, 'What is the value of the following 8-bit integer after all statements are executed?\r\nint x = 1;\r\nx = x << 7;\r\nx = x >> 7;\r\n', '1', '-1', '127', 'Implementation defined', 'Implementation defined'),
(50, 'Which of these expressions will make the rightmost set bit zero in an input integer x?\r\n', 'x = x | (x-1)', ' x = x & (x-1)', 'x = x | (x+1)', 'x = x | (x+1)', ' x = x & (x-1)'),
(51, ' Which of these expressions will isolate the rightmost set bit?\r\n', 'x = x & (~x)', 'x = x ^ (~x)', ' x = x & (-x)', 'x = x ^ (-x)', ' x = x & (-x)'),
(52, '0946, 786427373824, ‘x’ and 0X2f are _____, _____, ____ and _____ literals respectively\r\n', 'decimal, character,octal, hexadecimal', 'octal, hexadecimal, character, decimal', ' hexadecimal, octal, decimal, character', 'octal, decimal, character, hexadecimal', 'octal, decimal, character, hexadecimal'),
(53, 'What will be output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        int i = 3;\r\n        int l = i / -2;\r\n        int k = i % -2;\r\n        cout << l << k;\r\n        return 0;\r\n    }\r\n', 'compile time error', ' -1 1', '1 -1', 'implementation defined', ' -1 1'),
(54, 'What will be output of this function?\r\n\r\n    int main()\r\n    {\r\n        register int i = 1;\r\n        int *ptr = &i;\r\n        cout << *ptr;\r\n	return 0;\r\n    }\r\n', '0', ' 1', 'Compiler error may be possible', 'Runtime error may be possible', 'Compiler error may be possible'),
(55, 'Choose the correct option.\r\n    extern int i;\r\n    int i;\r\n', ' both 1 and 2 declare i', '1 declares the variable i and 2 defines i', '1 declares and defines i, 2 declares i', '1 declares i,2 declares and defines i', '1 declares i,2 declares and defines i'),
(56, ' Pick the right option\r\n    Statement 1:A definition is also a declaration.\r\n    Statement 2:An identifier can be declared just once.\r\nStatement 2 is true, Statement 1 is false.', 'Statement 1 is true, Statement 2 is false.', 'Statement 2 is true, Statement 1 is false.', 'Both are false.', 'Both are true.', 'Statement 2 is true, Statement 1 is false.'),
(57, ' Which of the given statements are false.\r\n1. extern int func;\r\n2. extern int func2(int,int);\r\n3. int func2(int,int);\r\n4. extern class foo;\r\n', '3 and 4 only', '2 and 3 only', 'only 4', ' 2, 3 and 4', 'only 4'),
(58, ' Pick the right option\r\n    Statement 1:Global values are not initialized by the stream.\r\n    Statement 2:Local values are implicitly initialised to 0.\r\n', 'Statement 1 is true, Statement 2 is false.', 'Statement 2 is true, Statement 1 is false.', 'Both are false.', 'Both are true.', 'Both are false.'),
(59, 'Can two functions declare variables(non static) with the same name.\r\n ', 'No', 'Yes', 'Yes, but not a very efficient way to write ', 'No, it gives a runtime error.', 'Yes, but not a very efficient way to write '),
(60, 'What is the output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    void addprint()\r\n    {\r\n        static int s = 1;\r\n        s++;\r\n        cout << s;\r\n    }\r\n    int main()\r\n    {\r\n        addprint();\r\n        addprint();\r\n        addprint();\r\n        return 0;\r\n    }\r\n', '234', '111', '123', '235', '111'),
(61, ' Identify the incorrect statements.\r\n    int var = 10;\r\n    int *ptr = &(var + 1); //statement 1\r\n    int *ptr2 = &var; //statement 2\r\n    &var = 40; //statement 3\r\n', 'Statement 1 and 2 are wrong', 'Statement 2 and 3 are wrong', 'Statement 1 and 3 are wrong', 'All the three are wrong', 'Statement 1 and 3 are wrong'),
(62, 'Identify the type of the variables.\r\n    typedef char* CHAR;\r\n    CHAR p,q;\r\n', ' char*', 'char', 'CHAR', ' unknown', ' char*'),
(63, ' Identify the incorrect option.\r\n', ' enumerators are constants', 'enumerators are user defined types', 'enumerators are same as macros', 'enumerator values start from 0 by default', 'enumerators are same as macros'),
(64, 'In which type does the enumerators are stored by the compiler?\r\n ', 'string', 'integer', ' float', 'none of the mentioned', 'integer'),
(65, 'To which of these enumerators can be assigned?\r\n', 'integer', 'negative', ' enumerator', 'all of the mentioned', 'all of the mentioned'),
(66, 'What will happen when defining the enumerated type?\r\n', ' it will not allocate memory', ' it will allocate memory', ' it will not allocate memory to its variables', ' none of the mentioned', ' it will not allocate memory'),
(67, ' Which variable does equals in size with enum variable?\r\n', 'int variable', ' float variable', 'string variable', 'none of the mentioned', 'int variable'),
(68, 'What is the output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    enum test {\r\n        A = 32, B, C\r\n    };\r\n    int main()\r\n    {\r\n        cout << A << B<< C;\r\n        return 0;\r\n    }\r\n ', ' 323334', '323232', '323130', 'none of the mentioned', ' 323334'),
(69, 'What is the output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    enum colour {\r\n        green, red, blue, white, yellow, pink\r\n    };\r\n    int main()\r\n    {\r\n        cout << green<< red<< blue<< white<< yellow<< pink;\r\n        return 0;\r\n    }\r\n', '012345', ' 123456', 'compile time error', 'runtime error', '012345'),
(70, 'What is output of the this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        int i;\r\n        enum month {\r\n            JAN = 1, FEB, MAR, APR, MAY, JUN, JUL, AUG, SEP, OCT, NOV, DEC\r\n        };\r\n        for (i = MAR; i <= NOV; i++)\r\n            cout << i;\r', '01234567891011', '123456789101112', '34567891011', '123456789', '34567891011'),
(71, 'Which of the following will not return a value?\r\n', 'null', ' void', ' empty', 'free', ' void'),
(72, ' ____ have the return type void?\r\n', 'all functions', 'constructors', 'destructors', 'none of the mentioned', 'none of the mentioned'),
(73, ' What does the following statement mean?\r\n    void a;\r\n', 'variable a is of type void', ' a is an object of type void', 'declares a variable with value a', 'flags an error', 'flags an error'),
(74, 'Choose the incorrect option\r\n ', 'void is used when the function does not return a v', 'void is also used when the value of a pointer is n', 'void is used as the base type for pointers to obje', 'void is a special fundamental type.', 'void is also used when the value of a pointer is n'),
(75, 'What is the output of this program?\r\n\r\n    #include <iostream>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        void a = 10, b = 10;\r\n        int c;\r\n        c = a + b;\r\n        cout << c;\r\n        return 0;\r\n    }\r\n', '20', 'compile time error', 'runtime error', 'none of the mentioned', 'compile time error'),
(76, 'What is meaning of following declaration?\r\nint(*p[5])();\r\n', 'p is pointer to function.', 'p is array of pointer to function.', 'p is pointer to such function which return type is', 'p is pointer to array of function.', 'p is array of pointer to function.'),
(77, ' What is size of generic pointer in C++ (in 32-bit platform) ?\r\n', ' 2', '4', ' 8', '0', '4'),
(78, 'What is the output of this program?\r\n\r\n   #include <iostream>\r\n   using namespace std;\r\n   int main()\r\n   {\r\n       int a[2][4] = {3, 6, 9, 12, 15, 18, 21, 24};\r\n       cout << *(a[1] + 2) << *(*(a + 1) + 2) << 2[1[a]];\r\n       return 0;\r\n   }\r\n', '15 18 21', '21 21 21', '24 24 24', 'Compile time error', '21 21 21'),
(79, ' What is the output of this program?\r\n\r\n   #include <iostream>\r\n   using namespace std;\r\n   int main()\r\n   {\r\n       int i;\r\n       char *arr[] = {"C", "C++", "Java", "VBA"};\r\n       char *(*ptr)[4] = &arr;\r\n       cout << ++(*ptr)[2];\r\n       return 0;\r\n   }\r\n', 'ava', ' java', 'c++', 'compile time error', 'ava'),
(80, 'What is the output of this program?\r\n\r\n   #include <iostream>\r\n   using namespace std;\r\n   int main()\r\n   {\r\n       int arr[] = {4, 5, 6, 7};\r\n       int *p = (arr + 1);\r\n       cout << *p;\r\n       return 0;\r\n   }\r\n', '4', '5', '6', '7', '5'),
(81, 'What is the output of this program?\r\n\r\n   #include <iostream>\r\n   using namespace std;\r\n   int main()\r\n   {\r\n       int arr[] = {4, 5, 6, 7};\r\n       int *p = (arr + 1);\r\n       cout << arr;\r\n       return 0;\r\n   }\r\n', ' 4', '5', 'address of arr', '7', 'address of arr'),
(82, 'What is the output of this program?\r\n\r\n   #include <iostream>\r\n   using namespace std;\r\n   int main()\r\n   {\r\n        int arr[] = {4, 5, 6, 7};\r\n        int *p = (arr + 1);\r\n        cout << *arr + 9;\r\n        return 0;\r\n   }\r\n ', '12', '5', '13', 'error', '13'),
(83, 'What is the main feature of locale in C++?\r\n', 'Sustanability', 'Portability', 'Reliability', 'None of the mentioned', 'Portability'),
(84, 'Which objects information is loaded in locale object?\r\n', ' facet object', 'instead object', 'Both a & b', 'None of the mentioned', ' facet object'),
(85, ' How many categories are available in facets?\r\n', '4', '5', '6', '3', '6'),
(86, 'What is the output of this program?\r\n\r\n    #include <stdio.h> \r\n    #include <ctype.h>\r\n    int main ()\r\n    {\r\n        int i = 0;\r\n        char str[] = "Steve Jobs\n";\r\n        char c;\r\n        while (str[i])\r\n        {\r\n            c = str[i];\r\n            if (islower(c)) \r\n                c = toup', 'Steve jobs', 'STEVE JOBS', 'Steve', 'None of the mentioned', 'STEVE JOBS'),
(87, 'What is the output of this program?\r\n\r\n    #include <stdio.h>\r\n    #include <ctype.h>\r\n    int main ()\r\n    {\r\n        int i;\r\n        char str[] = "jobs...";\r\n        i = 0;\r\n        while ( isalnum(str[i] )) \r\n            i++;\r\n        printf (" %d\n",i);\r\n        return 0;\r\n    }\r\n', '1', '2', '3', ' 4', ' 4'),
(88, 'What is the output of this program?\r\n\r\n    #include <stdio.h>\r\n    #include <ctype.h>\r\n    int main ()\r\n    {\r\n        int i = 0;\r\n        int cx = 0;\r\n        char str[] = "Hello, welcome!";\r\n        while (str[i])\r\n        {\r\n            if (ispunct(str[i])) cx++;\r\n                i++;\r\n        } ', '1', '2', '3', '4', '2'),
(89, 'What is the output of this program?\r\n    #include <stdio.h>\r\n    #include <stdlib.h>\r\n    #include <ctype.h>\r\n    int main ()\r\n    {\r\n        char str[] = "ffff";\r\n        long int number;\r\n        if (isxdigit(str[0]))\r\n        {\r\n            number = strtol (str, NULL, 16);\r\n            printf ("%', '64345', '21312', '65535', 'Error', '65535'),
(90, 'What kind of locale does every program is having in C++?\r\n', 'local locale', 'global locale', 'temp locale', 'None of the mentioned', 'global locale'),
(91, 'What will the monetary facet will do?\r\n ', 'Handle formatting and parsing of monetary values', 'Handle formatting and parsing of character values', 'Both a & b', 'None of the mentioned', 'Handle formatting and parsing of monetary values'),
(92, 'Which of the following correctly declares an array?\r\n', ' int array[10];', ' int array;', 'array{10};', 'array array[10];', ' int array[10];'),
(93, ' What is the index number of the last element of an array with 9 elements?\r\n', '9', '8', '0', 'Programmer-defined', '8'),
(94, 'What is a array?\r\n', 'An array is a series of elements of the same type ', 'An array is a series of element', 'An array is a series of elements of the same type ', 'None of the mentioned', 'An array is a series of elements of the same type '),
(95, ' Which of the following accesses the seventh element stored in array?\r\n', 'array[6];', 'array[7];', 'array(7);', 'array;', 'array[6];'),
(96, 'Which of the following gives the memory address of the first element in array?\r\n', 'array[0];', 'array[1];', 'array(2);', ' array;', ' array;'),
(97, 'What will be the output of the this program?\r\n\r\n    #include <stdio.h>\r\n    using namespace std;\r\n    int main ()\r\n    {\r\n        int array[] = {0, 2, 4, 6, 7, 5, 3};\r\n        int n, result = 0;\r\n        for (n = 0; n < 8; n++) {\r\n            result += array[n];\r\n        }\r\n        cout << result;\r\n', '25', ' 26', '27', 'None of the mentioned', 'None of the mentioned'),
(98, 'What is the output of this program?\r\n\r\n    #include <stdio.h>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        int a = 5, b = 10, c = 15;\r\n        int arr[3] = {&a, &b, &c};\r\n        cout << *arr[*arr[1] - 8];\r\n        return 0;\r\n    }\r\n', '15', '18', 'garbage value', 'compile time error', 'compile time error'),
(99, 'What is the output of this program?\r\n\r\n    #include <stdio.h>\r\n    using namespace std;\r\n    int main()\r\n    {\r\n        int array[] = {10, 20, 30};\r\n        cout << -2[array];\r\n        return 0;\r\n    }\r\n', '-15', '-30', 'compile time error', ' garbage value', '-30'),
(100, 'Choose the right option\r\n    string* x, y;\r\n', 'x is a pointer to a string, y is a string', 'y is a pointer to a string, x is a string', 'both x and y are pointer to string types', 'none of the mentioned', 'x is a pointer to a string, y is a string');

-- --------------------------------------------------------

--
-- Table structure for table `c_sharp`
--

CREATE TABLE `c_sharp` (
  `ID` int(11) NOT NULL,
  `Question` varchar(300) NOT NULL,
  `A` varchar(50) NOT NULL,
  `B` varchar(50) NOT NULL,
  `C` varchar(50) NOT NULL,
  `D` varchar(50) NOT NULL,
  `Answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `c_sharp`
--

INSERT INTO `c_sharp` (`ID`, `Question`, `A`, `B`, `C`, `D`, `Answer`) VALUES
(1, 'When does a structure variable get destroyed?\r\n', 'When no reference refers to it,it will get garbage', 'Depends on whether it is created using new or with', 'As variable goes out of the scope', 'Depends on either we free its memory using free() ', 'As variable goes out of the scope'),
(2, 'Choose the correct statement among the following which supports the fact that C# does not allow the creation of empty structures?\r\n', 'C#.NET supports creation of abstract user-defined ', 'By having empty structures,it would mean that the ', 'Basic reason to create structures is the inability', 'All of the above mentioned', 'All of the above mentioned'),
(3, 'aWhich of the following is a correct statement about the C#.NET code given below?\r\n\r\nstruct book\r\n{\r\n    private String name;\r\n    private int pages;\r\n    private Single price;\r\n}\r\nbook b = new book();\r\n', 'New structure can be inherited from struct book', 'When the program terminates, variable b will get g', 'The structure variable ‘b’ will be created on the ', 'When the program terminates, variable b will get g', 'The structure variable ‘b’ will be created on the '),
(4, 'Calculate the number of bytes a structure variable s occupies in the memory if it is defined as follows.\r\n\r\n  class abc\r\n  {\r\n      int i;\r\n      Decimal d;\r\n  }\r\n  struct sample\r\n  {\r\n     private int x;\r\n     private Single y;\r\n     private trial z;\r\n  }\r\n  sample s = new sample();\r\n', '24 bytes', '8 bytes', '16 bytes', '12 bytes', '12 bytes'),
(5, 'Choose the correct statement about structures in C#.NET?\r\n', 'Structures can be declared within a procedure', 'Structures can implement an interface but they can', 'Structure members cannot be declared as protected', 'A structure can be empty', 'Structures can implement an interface but they can'),
(6, 'The correct way to define a variable of type struct abc among the following is?\r\n\r\n struct abc\r\n { \r\n     public string name;\r\n     protected internal int age;\r\n     private Single sal;\r\n }\r\n', 'abc e = new abc();', 'abc e;', 'abc e = new abc;', '= new abc;', 'abc e = new abc();'),
(7, 'Select the correct statements among the following?\r\n', 'A structure can contain properties', 'A structure can contain constructors', ' A structure can contain protected data members', 'A structure cannot contain constants', 'A structure cannot contain constants'),
(8, 'Which of the following is the correct result for the given statement in the C#.NET statement given below?\r\np = q\r\n\r\nstruct employee\r\n{\r\n    private int employee id;\r\n    private string city;\r\n}\r\nemployee q = new employee();\r\nemployee p;\r\np = q;\r\n', 'Elements of ‘q’ will be copied into corresponding ', ' Address stored in q will get copied into p', 'Address of first element of q will get copied into', 'Once assignment is over.q will go out of scope and', 'Once assignment is over.q will go out of scope and'),
(9, 'Which of the following will be the correct output for the program given below?\r\n\r\n {\r\n     struct abc\r\n     {\r\n         int i;\r\n     }\r\n     class Program\r\n     {\r\n         static void Main(string[] args)\r\n         {\r\n             abc x = new abc();\r\n             abc z;\r\n             x.i = 10;\r\n    ', '10 10', '10 15', '15 10', '15 15', '10 15'),
(10, 'Which of these keywords are used to implement synchronization?\r\n', 'synchronize', ' syn', 'synch', 'synchronized', 'synchronized'),
(11, 'Which keyword is used for using the synchronization features defined by the Monitor class?\r\n ', ' lock', 'synchronized', 'Monitor', 'locked', ' lock'),
(12, 'What is synchronization in reference to a thread?\r\n', ' Its a process of handling situations when two or ', 'Its a process by which many thread are able to acc', 'Its a process by which a method is able to access ', ' Its a method that allow to many threads to access', 'Its a process of handling situations when two or m'),
(13, 'Which method is called when a thread is blocked from running temporarily?\r\n', 'Pulse()', 'PulseAll()', 'Wait()', 'Both b & c', 'Wait()'),
(14, 'What kind of exception is being thrown if Wait(),Pulse() or PulseAll() iscalled from code that is not within synchronized code?\r\n', ' System I/O Exception', 'DivideByZero Exception', 'SynchronizationLockException', 'All of the mentioned', 'SynchronizationLockException'),
(15, 'What is mutex?\r\n', 'a mutually exclusive synchronization object', ' can be acquired by more than one thread at a time', 'helps in sharing of resource which can be used by ', 'All of the mentioned', 'a mutually exclusive synchronization object'),
(16, 'What is Semaphore?', 'Grant more than one thread access to a shared reso', 'Useful when a collection of resources is being syn', 'Make use of a counter to control access to a share', ' all', ' all'),
(17, 'Which of these statements is incorrect?\r\n', 'By multithreading CPU’s idle time is minimized, an', 'By multitasking CPU’s idle time is minimized, and ', 'Two thread in Csharp can have same priority', 'A thread can exist only in two states, running and', 'By multithreading CPU’s idle time is minimized, an'),
(18, 'What is multithreaded programming?\r\n', 'It’s a process in which two different processes ru', 'It’s a process in which two or more parts of same ', 'Its a process in which many different process are ', 'Its a process in which a single process can access', 'It’s a process in which two or more parts of same '),
(19, 'What does URL stand for?\r\n', 'Uniform Resource Locator', 'Uniform Resource Latch', 'Universal Resource Locator', 'Universal Resource Latch', 'Universal Resource Latch'),
(20, 'Which of these exceptions is thrown by the URL class’s constructors?\r\n', 'URLNotFound', ' URLSourceNotFound', 'MalformedURLException', 'URLNotFoundException', 'URLNotFoundException'),
(21, 'What does the following form define?\r\nProtocol://HostName/FilePath?Query\r\n', 'Protocol specifies the protocol being used, such a', 'HostName identifies a specific server, such as mhp', 'FilePath specifies the path to a specific file', 'Query specifies information that will be sent to t', 'Protocol specifies the protocol being used, such a'),
(22, 'Which of these classes is used to encapsulate IP address and DNS?\r\n', 'DatagramPacket', 'URL', 'InetAddress', 'ContentHandler', 'ContentHandler'),
(23, 'Which of these is a standard for communicating multimedia content over email?\r\n', 'http', 'https', 'Mime', 'httpd', 'Mime'),
(24, 'What does the following method specify?\r\npublic static WebRequest Create(string requestUriString)\r\n', ' Creates a WebRequest object for the URI specified', 'The object returned will implement the protocol sp', 'The object will be an instance of the class that i', 'All of the mentioned', 'All of the mentioned'),
(25, 'What does the following code display while execution?\r\n\r\n class Program\r\n {\r\n     static void Main(string[] args) \r\n     {\r\n         Uri obj = new Uri("http://www.sanfoundry.com/csharpmcq");\r\n         Console.WriteLine(obj.AbsoluteUri);\r\n         Console.ReadLine();\r\n     }\r\n }\r\n', ' sanfoundry', 'sanfoundry.com', 'www.sanfoundry.com', ' http://www.sanfoundry.com/csharpmcq', ' http://www.sanfoundry.com/csharpmcq'),
(26, 'Which of these data members of HttpResponse class is used to store the response from a http server?\r\n', 'status', 'address', 'statusResponse', ' statusCode', ' statusCode'),
(27, 'Which of these classes is used to access actual bits or content information of a URL?\r\n', 'URL', 'URLDecoder', 'URLConnection', 'All of the mentioned', 'All of the mentioned'),
(28, 'What exception is thrown if the URI format is invalid?\r\n', 'URLNotFound', 'URLSourceNotFound', 'MalformedURLException', ' UriFormatException', 'UriFormatException'),
(29, 'What exception is thrown if the protocol supported by URI prefix is invalid?\r\n', 'URLNotFound', 'URLSourceNotFound', 'UriFormatException', 'NotSupportedException', 'NotSupportedException'),
(30, 'What exception is thrown if the user does not have a proper authorization?\r\n', ' URLNotFound', ' URLSourceNotFound', 'System.Security.SecurityException', 'All of the mentioned', 'All of the mentioned'),
(31, 'Choose the exceptions generated by the Create() method defined by WebRequest:\r\n', 'NotSupportedException', 'UriFormatException', 'System.Security.SecurityException', 'ArgumentNullException', 'ArgumentNullException'),
(32, 'Choose the exceptions generated by the GetReponse() method defined by WebRequest:\r\n', 'InvalidOperationException', 'ProtocolViolationException', 'WebException', 'UriFormatException', 'UriFormatException'),
(33, 'Select the two properties related to the network errors generated by WebException:\r\n ', 'Response', 'get', 'Status', 'set', 'Status'),
(34, 'Which of these classes is used for operating on the request from the client to the server?\r\n', 'http', 'httpDecoder', 'httpConnection', 'httpd', 'httpd'),
(35, 'Choose the exceptions generated by the GetResponseStream() method defined by WebRequest:\r\n', 'ProtocolViolationException', ' ObjectDisposedException', 'UriFormatException', ' IOException', ' IOException'),
(36, 'Which of these classes is used to create servers that listen to either local or remote client programs?\r\n', 'httpServer', 'ServerSockets', 'MimeHeader', 'HttpResponse', 'HttpResponse'),
(37, 'Which of these methods gives the full URL of an URL object?\r\n', 'fullHost()', 'getHost()', 'AbsoluteUri', 'toExternalForm()', 'fullHost()'),
(38, 'Which of these classes is used to read and write bytes in a file?\r\n', 'FileReader', 'FileWriter', 'FileInputStream', 'nputStreamReader', 'FileReader'),
(39, 'Which of these data types is returned by every method of OutputStream?\r\n', ' int', 'float', ' byte', 'None of the mentioned', 'None of the mentioned'),
(40, 'Which of these classes is used for input and output operation when working with bytes?\r\n', 'InputStream', 'Reader', 'Writer', 'All of the mentioned', 'InputStream'),
(41, 'Which among the following is used for storage of memory aspects?\r\n ', ' BufferedStream', 'FileStream', 'MemoryStream', 'None of the mentioned', 'None of the mentioned'),
(42, 'Which among the following is used for storage of unmanaged memory aspects?\r\n ', ' BufferedStream', 'FileStream', 'MemoryStream', 'UnmanagedMemoryStream', 'UnmanagedMemoryStream'),
(43, 'Which property among the following represents the current position of the stream?\r\n ', 'long Position', 'All of the mentioned', 'int Length', ' long Length', 'int Length'),
(44, ' Choose the filemode method which is used to create a new output file with the condition that the file with same name must not exist.\r\n', 'FileMode.CreateNew', 'FileMode.Create', 'FileMode.OpenOrCreate', 'FileMode.Truncate', 'FileMode.CreateNew'),
(45, 'Choose the filemode method which is used to create a new output file with the condition that the file with same name if exists will destroy the old file:\r\n', 'FileMode.CreateNew', 'FileMode.Create', ' FileMode.OpenOrCreate', 'FileMode.Truncate', 'FileMode.Truncate'),
(46, 'Which of these is a method used to clear all the data present in output buffers?\r\n', 'clear()', ' flush()', 'fflushx()', 'close()', ' flush()'),
(47, ' Which of these is a method used for reading bytes from the file?\r\n', 'Read()', 'ReadByte()', ' put()', 'write()', 'Read()'),
(48, 'Choose the statement which defines the Nullable type Correctly:\r\n', 'a special version of a value type that is represen', 'a nullable type can also store the value null', 'Nullable types are objects of System.Nullable, whe', 'None of the mentioned', 'None of the mentioned'),
(49, 'Choose the correct statement about process-based multitasking:\r\n', ' a feature that allows our computer to run two or ', 'a program that acts as a small unit of code that c', 'Only b', 'Both a & b', 'Both a & b'),
(50, 'Choose the statements which indicate the differences between the thread based multitasking and process based multitasking:\r\n', '', 'Process-based multitasking handles the concurrent ', 'Thread-based multitasking handles the concurrent e', 'Thread-based multitasking deals with the concurren', 'Thread-based multitasking deals with the concurren'),
(51, 'What is the advantage of the multithreading program?\r\n', 'Enables to utilize the idle and executing time pre', 'Enables to utilize the idle time present in most p', 'Both a & b', 'Only b', 'Only b'),
(52, 'Select the two type of threads mentioned in the concept of multithreading:\r\n', 'foreground', ' background', ' only a', 'Both a & b', 'Both a & b'),
(53, ' Number of threads that exists for each of the processes that occurs in the program:\r\n', 'atmost 1s', 'atleast 1', 'only 1', 'Both b & c', 'only 1'),
(54, 'Choose the namespace which supports multithreading programming:\r\n', 'System.net', 'System.Linq', 'System.Threading', 'All of the mentioned', 'System.Threading'),
(55, 'What does the given code snippet specify?\r\n\r\npublic Thread(ThreadStart start)\r\n', ' Defines a thread', 'Declaration of a thread constructor', 'Only a', 'Only a & b', 'Only a & b'),
(56, 'Which of these classes is used to make a thread?\r\n', 'String', 'System', 'Thread', 'Runnable', 'Thread'),
(57, 'On call of which type of method the new created thread will not start executing?\r\n', 'Begin()', 'Start()', 'New()', 'All of the mentioned', 'Start()'),
(58, ' Which of these method of Thread class is used to Suspend a thread for a period of time?\r\n', 'sleep()', ' terminate()', 'suspend()', 'stop()', 'sleep()'),
(59, ' The ‘ref’ keyword can be used with which among the following?\r\n', 'static function/subroutine', 'static data', ' Instance function/subroutine', ' instance data', ' Instance function/subroutine'),
(60, 'To implement delegates, the necessary condition is?\r\n', 'class declaration', ' inheritance', ' run time polymorphism', ' exceptions', 'class declaration'),
(61, ' Suppose a Generic class called as SortObjects is to be made capable of sorting objects of any type(integer, single, byte etc).Then, which of the following programming\r\nconstructs is able to implement the comparison function?\r\n', ' interface', 'encapsulation', 'delegate', 'attribute', 'delegate'),
(62, 'To generate a simple notification for an object in runtime, the programming construct to be used for implementing this idea?\r\n', 'namespace', 'interface', 'delegate', 'attribute', 'delegate'),
(63, 'Choose the incorrect statement among the following about the delegate?\r\n', 'delegates are of reference types', 'delegates are object oriented', 'delegates are type safe', 'none of the mentioned', 'none of the mentioned'),
(64, 'Which among the following is the correct statement about delegate declaration ?\r\ndelegate void del(int i);\r\n', 'On declaring the delegate, a class called del is c', 'the del class is derived from the MulticastDelegat', 'the del class will contain a one argument construc', 'none of the mentioned', 'On declaring the delegate, a class called del is c'),
(65, ' Which of the following is an incorrect statement about delegate?\r\n', 'A single delegate can invoke more than one method', 'delegates could be shared', 'delegates are type safe wrappers for function poin', 'delegate is a value type', 'delegates are type safe wrappers for function poin'),
(66, 'Which among the following differentiates a delegate in C#.NET from a conventional function pointer in other languages?\r\n', 'delegates in C#.NET represent a new type in the Co', 'delegates allows static as well as instance method', 'delegates are type safe and secure', 'none of the mentioned', 'none of the mentioned'),
(67, 'Choose the incorrect statement about delegates?\r\n', 'delegates are not type safe', 'delegates cannot be used to implement callback not', 'delegate is a user defined type', 'delegates permits execution of a method in an asyn', 'delegates are not type safe'),
(68, 'Which of the following statements is correct about a delegate?\r\n', ' inheritance is a prerequisite for using delegates', 'delegates are type safe', 'delegates provides wrappers for function pointers', 'none of the mentioned', 'none of the mentioned'),
(69, 'What does the following code depicts?\r\n1. System.Nullable count;\r\n2. bool? done;\r\n', 'code 1 declares the objects of nullable of type Nu', 'code 2 declares a nullable type in much shorter an', 'Both a & b', 'None of the mentioned', 'Both a & b'),
(70, ' Which operator is commonly used to find the size of the type of C#?\r\n', 'size()', 'sizeof(type)', 'Both a & b', 'None of the mentioned', 'sizeof(type)'),
(71, 'What will be the output of given code snippet?\r\n unsafe struct FixedBankRecord\r\n {\r\n     public fixed byte Name[80]; \r\n     public double Balance;\r\n     public long ID;\r\n }\r\n class UnsafeCode\r\n {\r\n     unsafe static void Main()\r\n     {\r\n         Console.WriteLine("Size of FixedBankRecord is " + size', 'Run time error', '80', '96', 'Compile time error', '96'),
(72, 'What will be the output of given code snippet?\r\n\r\nclass UnsafeCode\r\n{\r\n    unsafe static void Main()\r\n    {\r\n        int* ptrs = stackalloc int[3];\r\n        ptrs[0] = 1;\r\n        ptrs[1] = 2;\r\n        ptrs[2] = 3;\r\n        for (int i = 2; i >=0; i--)\r\n        Console.WriteLine(ptrs[i]);\r\n        Con', '3 2 1', '1 2 3', 'None of the mentioned', ' run time error', '3 2 1'),
(73, 'This section of our 1000+ C# multiple choice questions focuses on reference variables and assignments in C# Programming Language.\r\n\r\n1. Which refrence modifier is used to define reference variable?\r\n', ' &', 'ref', '#', '$', 'ref'),
(74, ' Select the output for following set of code :\r\n\r\n   static void Main(string[] args)\r\n   {\r\n       int a = 5;\r\n       fun1 (ref a);\r\n       Console.WriteLine(a);\r\n       Console.ReadLine();\r\n    }\r\n    static void fun1(ref int a)\r\n    {\r\n        a = a * a;\r\n    }\r\n', '5', ' 0', ' 20', '25', '25'),
(75, 'Select the output for the following set of code :\r\n\r\n static void Main(string[] args)\r\n {\r\n     int[] arr = new int[] {1 ,2 ,3 ,4 ,5 };\r\n     fun1(ref arr);\r\n     Console.ReadLine();\r\n  }\r\n static void fun1(ref int[] array)\r\n {\r\n     for (int i = 0; i < array.Length; i++)\r\n     {\r\n         array[i] ', '6 7 8 9 10', '5 17 8 8 20', '15 17 8 29 20', 'Syntax error while passing reference of array vari', '6 7 8 9 10'),
(76, 'Select the correct statement about ‘ref’ keyword in C#?\r\n', 'References can be called recursively', 'The ‘ref’ keyword causes arguments to be passed by', 'When ‘ref’ are used, any changes made to parameter', 'All of above mentioned', 'The ‘ref’ keyword causes arguments to be passed by'),
(77, 'Select correct differences between ‘=’ and ‘==’ in C#.\r\n', '‘==’ operator is used to assign values from one va', '=’ operator is used to assign values from one vari', 'No difference between both operators', 'None of the mentioned', 'None of the mentioned'),
(78, 'Select the correct output for following set of code.\r\n\r\n  static void Main(string[] args)\r\n  {\r\n      int X = 0;\r\n      if (Convert.ToBoolean(X = 0))\r\n      Console.WriteLine("It is zero");\r\n      else \r\n      Console.WriteLine("It is not zero");\r\n      Console.ReadLine();\r\n  }\r\n', ' It is zero', 'It is not zero', 'Infinite loop', 'None of the mentioned', ''),
(79, 'Select the output for the following set of code :\r\n\r\n    static void Main(string[] args)\r\n    {\r\n       int X = 6,Y = 2;\r\n       X  *= X / Y;\r\n       Console.WriteLine(X);\r\n       Console.ReadLine();\r\n    }\r\n ', '12', '6', 'Compile time error', '18', '18'),
(80, ' What is output for the following set of expression?\r\nint a+= (float) b/= (long)c.\r\n', ' float', ' int', 'long', 'None of the mentioned', 'None of the mentioned'),
(81, 'Which of the following is a correct statement about the C#.NET code given below?\r\n\r\nstruct book\r\n{\r\n    private String name;\r\n    private int pages;\r\n    private Single price;\r\n}\r\nbook b = new book();\r\n ', 'New structure can be inherited from struct book', 'When the program terminates, variable b will get g', 'The structure variable ‘b’ will be created on the ', 'When the program terminates, variable b will get g', 'New structure can be inherited from struct book'),
(82, ' Which of the following is a correct statement about the C#.NET code given below?\r\n\r\nclass trial\r\n{\r\n    int i;\r\n    float d;\r\n}\r\nstruct sample\r\n{\r\n    private int x;\r\n    private Single y;\r\n    private trial z;\r\n}\r\nsample s = new sample();\r\n', 'trial object referred by z is created on the stack', ') z is created on the heap', 'Both s and z will be created on the heap', 's will be created on the stack', 's will be created on the stack'),
(83, 'Which of the following is a correct statement about the C#.NET code given below?\r\n\r\nclass trial\r\n{\r\n    int i;\r\n    float d;\r\n}\r\nstruct sample\r\n{\r\n    private int x;\r\n    private Single y;\r\n    private trial z;\r\n}\r\nsample s = new sample();\r\n', 's will be created on the stack', 'Both s and z will be created on the heap', 'z is created on the heap', 'trial object referred by z is created on the stack', 's will be created on the stack'),
(84, 'Choose the correct statement about structures in C#.NET?\r\n', 'Structures can be declared within a procedure', 'Structures can implement an interface but they can', 'Structure members cannot be declared as protected', 'A structure can be empty', 'Structures can implement an interface but they can'),
(85, 'The correct way to define a variable of type struct abc among the following is?\r\n\r\n struct abc\r\n { \r\n     public string name;\r\n     protected internal int age;\r\n     private Single sal;\r\n }\r\n', ' abc e;', 'abc e;', ' new abc;', 'abc e = new abc;', 'abc e = new abc;'),
(86, 'When does a structure variable get destroyed?\r\n', 'When no reference refers to it,it will get garbage', 'Depends on whether it is created using new or with', 'As variable goes out of the scope', 'Depends on either we free its memory using free() ', 'Depends on whether it is created using new or with'),
(87, 'Calculate the number of bytes a structure variable s occupies in the memory if it is defined as follows.\r\n\r\n  class abc\r\n  {\r\n      int i;\r\n      Decimal d;\r\n  }\r\n  struct sample\r\n  {\r\n     private int x;\r\n     private Single y;\r\n     private trial z;\r\n  }\r\n  sample s = new sample();\r\n', '24 bytes', '8 bytes', '16 bytes', '12 bytes', '12 bytes'),
(88, 'Select the correct statements among the following?\r\n', ' A structure can contain properties', 'A structure can contain constructors', ' A structure can contain protected data members', 'A structure cannot contain constants', 'A structure can contain constructors'),
(89, 'Which of the following is the correct result for the given statement in the C#.NET statement given below?\r\np = q\r\n\r\nstruct employee\r\n{\r\n    private int employee id;\r\n    private string city;\r\n}\r\nemployee q = new employee();\r\nemployee p;\r\np = q;\r\n', 'Elements of ‘q’ will be copied into corresponding ', 'Address stored in q will get copied into p', 'Address of first element of q will get copied into', 'Once assignment is over.q will go out of scope and', 'Once assignment is over.q will go out of scope and'),
(90, 'Which of the following will be the correct output for the program given below?\r\n\r\n {\r\n     struct abc\r\n     {\r\n         int i;\r\n     }\r\n     class Program\r\n     {\r\n         static void Main(string[] args)\r\n         {\r\n             abc x = new abc();\r\n             abc z;\r\n             x.i = 10;\r\n    ', '15 10', '10 15', '15 15', '10 10', '10 15'),
(91, 'Which of the classes provide the operation of reading from and writing to the console in C#.NET?\r\n', 'System.Array', 'System.Output', 'System.ReadLine', 'System.Console', 'System.Console'),
(92, 'Which of the given stream methods provide access to the output console by default in C#.NET?\r\n', 'Console.In', 'Console.Out', 'Console.Error', 'All of the mentioned', 'All of the mentioned'),
(93, 'Which of the given stream methods provide the access to the input console in C#.NET?\r\n', 'Console.Out', 'Console.Error', 'Console.In', 'All of the mentioned', 'All of the mentioned'),
(94, 'The number of input methods defined by the stream method Console.In in C#.NET is?\r\n', '4', '3', '2', '1', '3'),
(95, 'Select the correct methodS provided by Console.In?\r\n', 'Read(), ReadLine()', 'ReadKey(), ReadLine()', 'Read(), ReadLine(), ReadKey()', 'ReadKey(), ReadLine()', 'Read(), ReadLine(), ReadKey()'),
(96, 'Choose the output returned when read() reads the character from the console?\r\n', 'String', 'Char', 'Integer', 'Boolean', 'Boolean'),
(97, 'Choose the output returned when error condition is generated while read() reads from the console.\r\n', 'False', '0', '-1', 'All of the mentioned', '-1'),
(98, 'Choose the object of TextReader class.\r\n', 'Console.In', 'Console.Out', ' Console.Error', 'None of the mentioned', 'None of the mentioned'),
(99, 'Choose the object/objects defined by the Textwriter class.\r\n', 'Console.In', 'Console.Out', ' Console.Error', 'None of the mentioned', ' Console.Error'),
(100, 'Choose the methods provided by Console.Out and Console.Error?\r\n', 'Write', 'WriteLine', 'WriteKey', 'All of the mentioned', 'Write');

-- --------------------------------------------------------

--
-- Table structure for table `java`
--

CREATE TABLE `java` (
  `ID` int(11) NOT NULL,
  `Question` varchar(300) NOT NULL,
  `A` varchar(50) NOT NULL,
  `B` varchar(50) NOT NULL,
  `C` varchar(50) NOT NULL,
  `D` varchar(50) NOT NULL,
  `Answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `java`
--

INSERT INTO `java` (`ID`, `Question`, `A`, `B`, `C`, `D`, `Answer`) VALUES
(1, 'What is the range of data type short in Java?', '-128 to 127', '-32768 to 32767', '-2147483648 to 2147483647', 'None of the mentioned', '-32768 to 32767'),
(2, 'What is the range of data type byte in Java?', '-128 to 127', '-32768 to 32767', '-2147483648 to 2147483647', 'None of the mentioned', '-128 to 127'),
(3, 'An expression involving byte, int, and literal numbers is promoted to which of these?', 'int', 'long', 'byte', 'float', 'int'),
(4, 'Which of these operators is used to allocate memory to array variable in Java?\r\n', 'malloc', 'alloc', 'new', ' new malloc', 'new'),
(5, 'Which of these is an incorrect array declaration?\r\n', 'int arr[] = new int[5', 'int [] arr = new int[5]', 'int arr[] arr = new int[5] ', 'int arr[] = int [5] new', 'int arr[] = int [5] new'),
(6, 'What will this code print?\r\nint arr[] = new int [5];\r\nSystem.out.print(arr);\r\n', ' 0 ', 'value stored in arr[0].', '00000', 'Garbage value', 'Garbage value'),
(7, ' Which of these is an incorrect Statement?\r\n', 'It is necessary to use new operator to initialize ', 'Array can be initialized using comma separated exp', ' Array can be initialized when they are declared.', 'None of the mentioned', 'initialize an array.'),
(8, ' Which of these is necessary to specify at time of array initialization?\r\n\r\n', 'Row', '\r\n\r\nColumn', '\r\n Both Row and Column', 'None of the mentioned', 'Row'),
(9, 'Which of these is necessary condition for automatic type conversion in Java?\r\n\r\n\r\n', 'The destination type is smaller than source type.', 'The destination type is larger than source type.', '\r\nThe destination type can be larger or smaller th', ' None of the mentioned', 'The destination type is larger than source type.'),
(10, 'What is the prototype of the default constructor of this class?\r\npublic class prototype { }\r\n\r\n\r\n\r\n', 'prototype( )', ' prototype(void)', ' public prototype(void)', ' public prototype( )', 'public prototype( )'),
(11, 'What is the error in this code?\r\nbyte b = 50;\r\nb = b * 50;\r\n', 'b can not contain value 100, limited by its range.', ' operator has converted b * 50 into int, which can', 'b can not contain value 50.', 'No error in this code', ' operator has converted b * 50 into int, which can'),
(12, 'If an expression contains double, int, float, long, then whole expression will promoted into which of these data types?\r\n', ' long', 'int', 'double', ' float', 'double'),
(13, 'What is Truncation is Java?\r\n', 'Floating-point value assigned to an integer type.', 'Integer value assigned to floating type.', 'Floating-point value assigned to an Floating type.', 'Integer value assigned to floating type.', 'Floating-point value assigned to an integer type.'),
(14, ' What is the output of this program?\r\n\r\n    class c {    \r\n        public void main( String[] args ) \r\n        {  \r\n            System.out.println( "Hello" + args[0] ); \r\n        } \r\n    }\r\n', 'Hello c', ' Hello', 'Hello world', 'Runtime Error.', 'Runtime Error.'),
(15, 'Which of these is data type long literal?\r\n', '0x99fffL', 'ABCDEFG', '0x99fffa', '99671246', '0x99fffL'),
(16, 'Which of these can be returned by the operator & ?\r\n', 'Integer', 'Boolean', 'Character', 'Integer or Boolean', 'Integer or Boolean'),
(17, 'Literals in java must be preceded by which of these?\r\n', ' L', ' l', 'D', 'L and I', 'L and I'),
(18, 'Literal can be of which of these data types?\r\n', 'integer', ' float', ' boolean', ' all of the mentioned', ' all of the mentioned'),
(19, 'Which of these can not be used for a variable name in Java?\r\n', ' identifier', 'keyword', 'identifier & keyword', ' None of the mentioned', ' keyword'),
(20, 'What is the output of this program?\r\n\r\n    class array_output {\r\n        public static void main(String args[]) \r\n        {\r\n       	    int array_variable [] = new int[10];\r\n	    for (int i = 0; i < 10; ++i) {\r\n                array_variable[i] = i/2;\r\n                array_variable[i]++;\r\n        ', '0 2 4 6 8', '1 2 3 4 5', '0 1 2 3 4 5 6 7 8 9', '1 2 3 4 5 6 7 8 9 10', '1 2 3 4 5'),
(21, 'What is the output of this program?\r\n\r\n    class variable_scope {\r\n        public static void main(String args[]) \r\n        {\r\n            int x;\r\n            x = 5;\r\n            {\r\n	        int y = 6;\r\n	        System.out.print(x + " " + y);\r\n            }\r\n            System.out.println(x + " " + ', ' 5 6 5 6', '5 6 5', ' Runtime error', 'Compilation error', 'Compilation error'),
(22, 'Which of these is incorrect string literal?\r\n', '“Hello World”', ') “Hello\nWorld”', ') “”Hello World””', 'Hello\r\nworld”', 'Hello\r\nworld”'),
(23, 'What is the output of this program?\r\n\r\n    class dynamic_initialization {\r\n        public static void main(String args[]) \r\n        {\r\n            double a, b;\r\n            a = 3.0;\r\n            b = 4.0;\r\n	    double c = Math.sqrt(a * a + b * b);\r\n	    System.out.println(c);\r\n        } \r\n    }\r\n', '5.0', '25.0', '7.0', 'Compilation Error', '5.0'),
(24, ' What is the numerical range of a char in Java?\r\n', '-128 to 127', '0 to 256', '0 to 32767', '0 to 65535', '0 to 65535'),
(25, 'Which of these coding types is used for data type characters in Java?\r\na) ASCII\r\nb) ISO-LATIN-1\r\nc) UNICODE\r\nd) None of the mentionedWhich of these coding types is used for data type characters in Java?\r\na) ASCII\r\nb) ISO-LATIN-1\r\nc) UNICODE\r\nd) None of the mentionedWhich of these coding types is use', 'ASCII', 'ISO-LATIN-1', 'UNICODE', 'None of the mentioned', 'UNICODE'),
(26, 'Which of these values can a boolean variable contain?\r\n', 'True & False', '0 & 1', 'Any integer value', 'true', 'True & False'),
(27, 'Which of these occupy first 0 to 127 in Unicode character set used for characters in Java?\r\n', 'ASCII', 'ISO-LATIN-1', 'None of the mentioned', 'ASCII and ISO-LATIN1', 'ASCII and ISO-LATIN1'),
(28, ' Which one is a valid declaration of a boolean?\r\n', ' boolean b1 = 1;', 'boolean b2 = ‘false’;', 'boolean b3 = false;', 'boolean b4 = ‘true’', 'boolean b3 = false;'),
(29, ' What is the output of this program?\r\n\r\n    class booloperators {\r\n        public static void main(String args[]) \r\n        {\r\n            boolean var1 = true;\r\n	    boolean var2 = false;\r\n	    System.out.println((var2 & var2));\r\n        } \r\n    }\r\n\r\n', ' 0', '1', ' true', ' false', ' false'),
(30, ' What is the range of data type short in Java?\r\n', '-128 to 127', ' -32768 to 32767', '-2147483648 to 2147483647', 'None of the mentioned', ' -32768 to 32767'),
(31, 'What is the range of data type byte in Java?\r\n', '-128 to 127', '-32768 to 32767', '-2147483648 to 2147483647', 'None of the mentioned', '-128 to 127'),
(32, 'An expression involving byte, int, and literal numbers is promoted to which of these?\r\n', 'int', 'long', 'byte', 'float', 'int'),
(33, 'Which of these literals can be contained in a data type float variable?\r\n', '1.7e-308', '3.4e-038', '1.7e+308', '3.4e-050', '3.4e-038'),
(34, 'Which data type value is returned by all transcendental math functions?\r\n', ' int', 'float', ' double', 'long', ' double'),
(35, 'What is the output of this program?\r\n\r\n    class average {\r\n        public static void main(String args[])\r\n        {\r\n            double num[] = {5.5, 10.1, 11, 12.8, 56.9, 2.5};\r\n            double result;\r\n            result = 0;\r\n            for (int i = 0; i < 6; ++i) \r\n                result =', '16.34', '16.566666644', '16.46666666666667', '16.46666666666666', '16.46666666666667'),
(36, 'What is the output of this program?\r\n\r\n    class conversion {\r\n        public static void main(String args[]) \r\n        {    \r\n             double a = 295.04;\r\n             int  b = 300;\r\n             byte c = (byte) a;\r\n             byte d = (byte) b;\r\n             System.out.println(c + " "  + d);', '38 43', ' 39 44', '295 300', '295.04 300', ' 39 44'),
(37, 'What is the output of this program?\r\n\r\n    class area {\r\n        public static void main(String args[]) \r\n        {    \r\n             double r, pi, a;\r\n             r = 9.8;\r\n             pi = 3.14;\r\n             a = pi * r * r;\r\n             System.out.println(a);\r\n        } \r\n    }\r\n', ' 301.5656', ' 301', '301.56', '301.56560000', ' 301.5656'),
(38, ' Which of these selection statements test only for equality?\r\n', ' if', 'switch', ' if & switch', ' None of the mentioned', 'switch'),
(39, 'Which of these are selection statements in Java?\r\n', ' if()', 'for()', 'continue', 'break', ' if()'),
(40, 'Which of the following loops will execute the body of loop even when condition controlling the loop is initially false?\r\n', 'do-while', ' while', 'for', 'None of the mentioned', 'do-while'),
(41, 'Which of these jump statements can skip processing remainder of code in its body for a particular iteration?\r\n', 'break', ' return', 'exit', 'continue', 'continue'),
(42, 'Which of these statement is correct?\r\n', 'switch statement is more efficient than a set of n', 'two case constants in the same switch can have ide', 'switch statement is more efficient than a set swit', 'it is possible to create a nested switch statement', 'two case constants in the same switch can have ide'),
(43, 'What is the output of this program?\r\n\r\n    class comma_operator {\r\n        public static void main(String args[]) \r\n        {    \r\n             int sum = 0;\r\n             for (int i = 0, j = 0; i < 5 & j < 5; ++i, j = i + 1)\r\n                 sum += i;\r\n 	     System.out.println(sum);\r\n        } \r\n ', '5', '6', '14', 'compilation error', '6'),
(44, 'What is the output of this program?\r\n\r\n    class jump_statments {\r\n        public static void main(String args[]) \r\n        {        \r\n             int x = 2;\r\n             int y = 0;\r\n             for ( ; y < 10; ++y) {\r\n                 if (y % x == 0) \r\n                     continue;  \r\n         ', '1 3 5 7', '2 4 6 8', '1 3 5 7 9', '1 2 3 4 5 6 7 8 9', '1 3 5 7 9'),
(45, 'What is the output of this program?\r\n\r\n    class Output {\r\n        public static void main(String args[]) \r\n        {    \r\n             int a = 5;\r\n             int b = 10;\r\n             first: {\r\n                second: {\r\n                   third: { \r\n                       if (a ==  b >> 1)\r\n    ', '5 10', '10 5', '5', '10', '10'),
(46, 'Which of these have highest precedence?\r\n', '()', '++', '*', '>>', '()'),
(47, 'What should be expression1 evaluate to in using ternary operator as in this line?\r\nexpression1 ? expression2 : expression3\r\n', 'Integer', 'Floating – point numbers', ' Boolean', 'None of the mentioned', ' Boolean'),
(48, ' What is the value stored in x in following lines of code?\r\nint x, y, z;\r\nx = 0;\r\ny = 1;\r\nx = y = z = 8;\r\n', '0', '1', '9', '8', '8'),
(49, 'What is the order of precedence (highest to lowest) of following operators?\r\n1. &\r\n2. ^\r\n3. ?:\r\n', '1 -> 2 -> 3', ' 2 -> 1 -> 3', '3 -> 2 -> 1', ' 2 -> 3 -> 1', '1 -> 2 -> 3'),
(50, 'What is the output of this program?\r\n\r\n    class operators {\r\n        public static void main(String args[]) \r\n        {    \r\n             int x = 8;\r\n             System.out.println(++x * 3 + " " + x);\r\n        } \r\n    }\r\n', ' 24 8', '24 9', '27 8', '27 9', '27 9'),
(51, 'What is the output of this program?\r\n\r\n    class ternary_operator {\r\n        public static void main(String args[]) \r\n        {        \r\n             int x = 3;\r\n             int y = ~ x;\r\n             int z;\r\n             z = x > y ? x : y;\r\n             System.out.print(z);\r\n        } \r\n    }\r\n', '0', '1', ' 3', '-4', ' 3'),
(52, ' Which of these lines of code will give better performance?\r\n1. a | 4 + c >> b & 7;\r\n2. (a | ((( 4 * c ) >> b ) & 7 ))\r\n\r\n\r\n', '1 will give better performance as it has no parent', '2 will give better performance as it has parenthes', 'Both 1 & 2 will give equal performance.Both 1 & 2 ', 'Dependent on the computer system.', 'Both 1 & 2 will give equal performance.'),
(53, 'What is the output of this program?\r\n\r\n    class Output {\r\n        public static void main(String args[]) \r\n        {    \r\n             int x , y = 1;\r\n             x = 10;\r\n             if (x != 10 && x / 0 == 0)\r\n                 System.out.println(y);\r\n             else\r\n                 System.o', '1', '2', 'Runtime error owing to division by zero in if cond', 'Unpredictable behavior of program.', '2'),
(54, 'What is the output of relational operators?\r\n', 'Integer', 'Boolean', 'Characters', 'Double', 'Boolean'),
(55, ' What is the output of this program?\r\n\r\n    class ternary_operator {\r\n        public static void main(String args[]) \r\n        {        \r\n             int x = 3;\r\n             int y = ~ x;\r\n             int z;\r\n             z = x > y ? x : y;\r\n             System.out.print(z);\r\n        } \r\n    }\r\n', '0', '1', '3', ' -4', '3'),
(56, 'What is the output of this program?\r\n\r\n    class Output {\r\n        public static void main(String args[]) \r\n        {    \r\n             boolean a = true;\r\n             boolean b = false;\r\n             boolean c = a ^ b;\r\n             System.out.println(!c);\r\n        } \r\n    }\r\n', '0', '1', 'false', 'true', 'false'),
(57, ' What is the output of relational operators?\r\n', 'Integer', 'Boolean', ' Characters', 'Double', 'Boolean'),
(58, ' Which of these is not a bitwise operator?\r\n', '&', '&=', ') |=', '<= [expand title', '&'),
(59, ' What is the output of this program?\r\n\r\n    class bitwise_operator {\r\n        public static void main(String args[])\r\n        {\r\n            int var1 = 42;\r\n            int var2 = ~var1;\r\n            System.out.print(var1 + " " + var2);     	\r\n        } \r\n    }\r\n', ') 42 42', '43 43', '42 -43', '42 43', '42 -43'),
(60, 'What is the output of this program?\r\n    class bitwise_operator {\r\n        public static void main(String args[]) \r\n        {    \r\n             int a = 3;\r\n             int b = 6;\r\n 	     int c = a | b;\r\n             int d = a & b;             \r\n             System.out.println(c + " "  + d);\r\n      ', '7 2', '7 7', '5 6', '5 2', '7 2'),
(61, 'What is the output of this program?\r\n\r\n    class leftshift_operator {\r\n        public static void main(String args[]) \r\n        {        \r\n             byte x = 64;\r\n             int i;\r\n             byte y; \r\n             i = x << 2;\r\n             y = (byte) (x << 2)\r\n             System.out.print(', '0 64', '64 0', '0 256', '256 0', '256 0'),
(62, 'What is the output of this program?\r\n\r\n    class rightshift_operator {\r\n        public static void main(String args[]) \r\n        {    \r\n             int x; \r\n             x = 10;\r\n             x = x >> 1;\r\n             System.out.println(x);\r\n        } \r\n    }\r\n', '10', '5', '2', '20', '5'),
(63, 'What is the output of this program?\r\n\r\n    class Output {\r\n        public static void main(String args[]) \r\n        {    \r\n             int a = 1;\r\n             int b = 2;\r\n             int c = 3;\r\n             a |= 4;\r\n             b >>= 1;\r\n             c <<= 1;\r\n             a ^= c;\r\n            ', '3 1 6', '2 2 3', ' 2 3 4', '3 3 6', '3 1 6'),
(64, 'Which of the following can be operands of arithmetic operators?\r\n', 'Numeric', 'Boolean', 'Characters', 'Both Numeric & Characters', 'Both Numeric & Characters'),
(65, 'Modulus operator, %, can be applied to which of these?\r\n', 'Integers', 'Floating – point numbers', 'Both Integers and floating – point numbers.', 'None of the mentioned', 'Both Integers and floating – point numbers.'),
(66, 'With x = 0, which of the following are legal lines of Java code for changing the value of x to 1?\r\n1. x++;\r\n2. x = x + 1;\r\n3. x += 1;\r\n4. x =+ 1;\r\n', '1, 2 & 3', ' 1 & 4', '1, 2, 3 & 4', '3 & 2', '3 & 2'),
(67, ' Decrement operator, –, decreases value of variable by what number?\r\n', '1', '2', '2', '4', '1'),
(68, 'Which of these statements are incorrect?\r\n', 'Assignment operators are more efficiently implemen', 'Assignment operators run faster than their equival', 'Assignment operators can be used only with numeric', 'Which of these statements are incorrect?\r\na) Assig', 'Which of these statements are incorrect?\r\na) Assig'),
(69, 'What is the output of this program?\r\n\r\n    class increment {\r\n        public static void main(String args[])\r\n        {\r\n            double var1 = 1 + 5; \r\n            double var2 = var1 / 4;\r\n            int var3 = 1 + 5;\r\n            int var4 = var3 / 4;\r\n            System.out.print(var2 + " " + ', '1 1', ' 0 1', '1.5 1', '1.5 1.0', '1.5 1'),
(70, 'What is the output of this program?\r\n\r\n    class Modulus {\r\n        public static void main(String args[]) \r\n        {    \r\n             double a = 25.64;\r\n             int  b = 25;\r\n             a = a % 10;\r\n             b = b % 10;\r\n             System.out.println(a + " "  + b);\r\n        } \r\n    }', '5.640000000000001 5', '5.640000000000001 5.0', '5 5', '5 5.640000000000001', '5.640000000000001 5'),
(71, 'What is the output of this program?\r\n\r\n    class increment {\r\n        public static void main(String args[]) \r\n        {        \r\n             int g = 3;\r\n             System.out.print(++g * 8);\r\n        } \r\n    }\r\n', '25', '24', '32', '33', '32'),
(72, 'What is the output of this program?\r\n    class Output {\r\n        public static void main(String args[]) \r\n        {    \r\n             int x , y;\r\n             x = 10;\r\n             x++;\r\n             --x;\r\n             y = x++;\r\n             System.out.println(x + " " + y);\r\n        } \r\n    }\r\n', '11 11', ' 10 10', '11 10', '10 11', '11 10'),
(73, 'What is the output of this program?\r\n\r\n    class Output {\r\n        public static void main(String args[]) \r\n        {    \r\n             int a = 1;\r\n             int b = 2;\r\n             int c;\r\n             int d;\r\n             c = ++b;\r\n             d = a++;\r\n             c++;\r\n             b++;\r\n ', ' 3 2 4', '3 2 3', '2 3 4', '3 4 4', '3 4 4'),
(74, ' String in Java is a?\r\n', ' class', 'object', 'variable', 'character array', 'character array'),
(75, 'Which of these method of String class is used to obtain character at specified index?\r\n', ' char()', 'Charat()', 'charat()', 'charAt()', 'charAt()'),
(76, 'Which of these keywords is used to refer to member of base class from a sub class?\r\n', ' upper', 'super', ' this', 'None of the mentioned', 'super'),
(77, 'Which of these method of String class can be used to test to strings for equality?\r\n', 'sequal()', 'isequals()', 'equal()', 'equals()', 'equals()'),
(78, 'Which of the following statements are incorrect?\r\n', 'String is a class.', 'Strings in java are mutable.', 'Every string is an object of class String.', 'Java defines a peer class of String, called String', 'Strings in java are mutable.'),
(79, 'What is the output of this program?\r\n\r\n    class string_demo {\r\n        public static void main(String args[])\r\n        {\r\n            String obj = "I" + "like" + "Java";   \r\n            System.out.println(obj);     \r\n        }\r\n   }\r\n\r\n', ' I', ' like', 'Java', 'IlikeJava', 'IlikeJava'),
(80, 'What is the output of this program?\r\n\r\n    class string_class {\r\n        public static void main(String args[])\r\n        {\r\n            String obj = "I LIKE JAVA";   \r\n            System.out.println(obj.charAt(3));\r\n        } \r\n    }\r\n', 'I', 'L', 'K', 'E', 'I'),
(81, 'What is the output of this program?\r\n\r\n    class string_class {\r\n        public static void main(String args[])\r\n        {\r\n            String obj = "I LIKE JAVA";   \r\n            System.out.println(obj.length());\r\n        }\r\n    }\r\n', ' 9', '10', '11', '12', '11'),
(82, 'What is the output of this program?\r\n\r\n    class string_class {\r\n        public static void main(String args[])\r\n        {\r\n            String obj = "hello";\r\n            String obj1 = "world";   \r\n            String obj2 = "hello";\r\n            System.out.println(obj.equals(obj1) + " " + obj.equals', ' false false', 'true true', ' true false', 'false true', 'false true'),
(83, ' Arrays in Java are implemented as?\r\n', 'class', 'object', 'variable', 'None of the mentioned\r\n', 'object'),
(84, 'Which of these keywords is used to prevent content of a variable from being modified?\r\n', 'final', ' last', ' constant', 'static', 'final'),
(85, 'Which of these cannot be declared static?\r\n', 'class', 'object', 'variable', 'method', 'object'),
(86, 'Which of the following statements are incorrect?\r\n', 'static methods can call other static methods only.', 'static methods must only access static data.', 'static methods can not refer to this or super in a', 'when object of class is declared, each object cont', 'when object of class is declared, each object cont'),
(87, 'Which of the following statements are incorrect?\r\n', 'Variables declared as final occupy memory.', 'final variable must be initialized at the time of ', 'Arrays in java are implemented as an object.', 'All arrays contain an attribute-length which conta', 'Variables declared as final occupy memory.'),
(88, 'Which of these methods must be made static?\r\n', 'main()', 'delete()', 'run()', 'finalize()', 'main()'),
(89, 'What is the output of this program?\r\n\r\n    class access{\r\n       static int x;\r\n       void increment(){\r\n           x++;\r\n       }   \r\n     }   \r\n    class static_use {\r\n        public static void main(String args[])\r\n        {\r\n            access obj1 = new access();\r\n            access obj2 = new', ' 1 2', ' 1 1', '2 2', 'Compilation Error', '2 2'),
(90, 'What is the output of this program?\r\n\r\n    class static_out {\r\n        static int x;\r\n 	static int y;\r\n        void add(int a , int b){\r\n            x = a + b;\r\n            y = x + b;\r\n        }\r\n    }    \r\n    class static_use {\r\n        public static void main(String args[])\r\n        {\r\n          ', '7 7', '6 6', '7 9', '9 7', '7 9'),
(91, ' What is the output of this program?\r\n\r\n    class Output {\r\n        public static void main(String args[])\r\n        {\r\n            int arr[] = {1, 2, 3, 4, 5};\r\n            for ( int i = 0; i < arr.length - 2; ++i)\r\n                System.out.println(arr[i] + " ");\r\n        } \r\n    }\r\n\r\n', '1 2', ' 1 2 3', '1 2 3 4', '1 2 3 4 5', ' 1 2 3'),
(92, 'What is the output of this program?\r\n\r\n    class Output {\r\n        public static void main(String args[])\r\n        {\r\n            int a1[] = new int[10];\r\n            int a2[] = {1, 2, 3, 4, 5};\r\n            System.out.println(a1.length + " " + a2.length);\r\n        } \r\n    }\r\n', '10 5', ' 5 10', ' 0 10', '0 5', '10 5'),
(93, 'Which of these access specifiers must be used for main() method?\r\n', 'private', ' public', 'protected', 'None of the mentioned', ' public'),
(94, 'Which of these is used to access member of class before object of that class is created?\r\n', 'public', 'private', ' static', 'protected', ' static'),
(95, 'Which of these is used as default for a member of a class if no access specifier is used for it?\r\n', 'private', 'public', 'public, within its own package', 'protected', 'private'),
(96, 'What is the process by which we can control what parts of a program can access the members of a class?\r\n', 'Polymorphism', 'Abstraction', 'Encapsulation', 'Recursion', 'Encapsulation'),
(97, ' What is the output of this program?\r\n\r\n    class access{\r\n        public int x;\r\n 	private int y;\r\n        void cal(int a, int b){\r\n            x =  a + 1;\r\n            y =  b;\r\n        }        \r\n    }    \r\n    class access_specifier {\r\n        public static void main(String args[])\r\n        {\r\n  ', ' 3 3', '2 3', 'Runtime Error', 'Compilation Error', 'Runtime Error'),
(98, '\r\nWhat is process of defining two or more methods within same class that have same name but different parameters declaration?\r\n', 'method overloading', 'method overriding', 'method hiding', 'None of the mentioned', 'method overloading'),
(99, 'Which of these can be overloaded?\r\n', 'Methods', 'Constructors', 'All of the mentioned', 'None of the mentioned', 'All of the mentioned'),
(100, 'What is the process of defining a method in terms of itself, that is a method that calls itself?\r\n', 'Polymorphism', 'Abstraction', 'Encapsulation', 'Recursion', 'Recursion');

-- --------------------------------------------------------

--
-- Table structure for table `python`
--

CREATE TABLE `python` (
  `ID` int(11) NOT NULL,
  `Question` varchar(300) NOT NULL,
  `A` varchar(50) NOT NULL,
  `B` varchar(50) NOT NULL,
  `C` varchar(50) NOT NULL,
  `D` varchar(50) NOT NULL,
  `Answer` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `python`
--

INSERT INTO `python` (`ID`, `Question`, `A`, `B`, `C`, `D`, `Answer`) VALUES
(1, ' Which of the following CLI command can also be used to rename files ?\r\n', 'rm', 'mv', 'rm -r', 'None of the mentioned', 'mv'),
(2, 'Point out the correct statement :\r\n', 'CLI can help you to organize messages', 'CLI can help you to organize files and folders', ' Navigation of directory is possible using CLI', 'None of the Mentioned', 'CLI can help you to organize files and folders'),
(3, 'Which of the following command allows you to change directory to one level above your parent directory ?\r\n', 'cd', 'cd.', 'cd..', 'None of the Mentioned', 'cd..'),
(4, 'Which of the following is not a CLI command ?\r\n', 'delete', 'rm', 'clear', 'None of the Mentioned', 'delete'),
(5, 'Point out the wrong statement:\r\n', ' command is the CLI command which does a specific ', 'There is one and only flag for every command in CL', 'Flags are the options given to command for activat', 'All of the Mentioned', 'There is one and only flag for every command in CL'),
(6, 'Which of the following systems record changes to a file over time ?\r\n', 'Record Control', 'Version Control', 'Forecast Control', 'None of the mentioned', 'Version Control'),
(7, 'Which of the following is a revision control system ?\r\n', 'Git', 'NumPy', 'Slidify', 'None of the mentioned', 'Git'),
(8, 'Which of the following command line environment is used for interacting with Git ?\r\n', 'GitHub', 'Git Bash', 'Git Boot', 'All of the mentioned', 'Git Bash'),
(9, 'Which of the following web hosting service use Git control system ?\r\n', 'GitHub', 'Open Hash', ' Git Bash', 'None of the mentioned', 'GitHub'),
(10, ' cp command can be used to copy the content of directories.\r\n', ' cp command can be used to copy the content of dir', 'False', 'both ', 'none', 'True'),
(11, 'Point out the correct statement :\r\n', 'Least square is an estimation tool', ' Least square problems falls in to three categorie', 'Compound least square is one of the category of le', 'None of the Mentioned', 'Least square is an estimation tool'),
(12, ' How many principles of analytical graphs exist ?\r\n', '3', '4', '6', 'None of the Mentioned', '6'),
(13, 'Which of the following is not a step in data analysis ?\r\n', ' Obtain the data', 'Clean the data', 'EDA', 'None of the Mentioned', 'None of the Mentioned'),
(14, 'Point out the wrong statement:\r\n\r\n', 'Simple linear regression is equipped to handle mor', 'Compound linear regression is not equipped to hand', 'Linear regression consists of finding the best-fit', 'All of the Mentioned', 'Simple linear regression is equipped to handle mor'),
(15, 'Which of the following technique comes under practical machine learning ?\r\n', 'Bagging', 'Boosting', ' Forecasting', 'None of the Mentioned', 'Boosting'),
(16, 'Which of the following technique is also referred to as Bagging ?\r\n', 'Bootstrap aggregating', 'Bootstrap subsetting', ' Bootstrap predicting', 'All of the Mentioned', 'Bootstrap aggregating'),
(17, 'Which of the following is characteristic of Raw Data ?\r\n', 'Data is ready for analysis', 'Original version of data', 'Easy to use for data analysis', 'None of the Mentioned', 'Original version of data'),
(18, 'Point out the correct statement:\r\n', 'Raw data is original source of data', 'Preprocessed data is original source of data', 'Raw data is the data obtained after processing ste', 'None of the Mentioned', 'Raw data is original source of data'),
(19, 'Which of the following is performed by Data Scientist ?\r\n', 'Define the question', 'Create reproducible code', 'Challenge results', 'All of the Mentioned', 'All of the Mentioned'),
(20, 'Which of the following is most important language for Data Science ?\r\n', 'Java', 'Ruby', 'R', 'None of the Mentioned', 'R'),
(21, 'Point out the wrong statement:\r\n', 'Merging concerns combining datasets on the same ob', ' Data visualization is the organization of informa', 'Subsetting can be used to select and exclude varia', ' All of the Mentioned', ' Data visualization is the organization of informa'),
(22, 'Which of the following approach should be used to ask Data Analysis question ?\r\n', 'Find only one solution for particular problem', 'Find out the question which is to be answered', 'Find out answer from dataset without asking questi', 'None of the mentioned', 'Find out the question which is to be answered'),
(23, 'Which of the following is one of the key data science skill ?\r\n', 'Statistics', 'Machine Learning', 'Data Visualization', 'All of the Mentioned', 'All of the Mentioned'),
(24, 'Which of the following is key characteristic of hacker ?\r\n', 'Afraid to say they don’t know the answer', 'Willing to find answers on their own', 'Not Willing to find answers on their own', 'All of the mentioned', 'Willing to find answers on their own'),
(25, 'Which of the following is characteristic of Processed Data ?\r\n', 'Data is not ready for analysis', 'All steps should be noted', 'Hard to use for data analysis', ' None of the mentioned', 'All steps should be noted');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `c`
--
ALTER TABLE `c`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `c_plus_plus`
--
ALTER TABLE `c_plus_plus`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `c_sharp`
--
ALTER TABLE `c_sharp`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `java`
--
ALTER TABLE `java`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `python`
--
ALTER TABLE `python`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `c`
--
ALTER TABLE `c`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `c_plus_plus`
--
ALTER TABLE `c_plus_plus`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `c_sharp`
--
ALTER TABLE `c_sharp`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `java`
--
ALTER TABLE `java`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `python`
--
ALTER TABLE `python`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
